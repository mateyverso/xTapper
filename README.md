# xTapper

xTapper is a Firefox Web Extension that aims to extract structured data from twitter/x.

Related packages that assist in that objective are also included in this repository.

## Packages

### [xTapper](./xTapper/README.md)

The Firefox Web Extension.

### [xSyncServer](./xSyncServer/README.md)

NodeJS server that ingests the recorded data from xTapper and persists it to a sqlite db.

### [xTractor](./xTractor/README.md)

Library that extract tweets and users from twitter GraphQL responses. Provides Types and JSON Schemas.

## License

[GPLv3 License](./LICENSE.txt)

The licenses of external libraries can be found in their corresponding repository