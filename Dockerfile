FROM node:20-slim AS base
ENV PNPM_HOME="/pnpm"
ENV PATH="$PNPM_HOME:$PATH"
RUN corepack enable

FROM base AS build
COPY . /usr/src/app
WORKDIR /usr/src/app
RUN --mount=type=cache,id=pnpm,target=/pnpm/store pnpm install --frozen-lockfile
# RUN pnpm run -r build
RUN pnpm run --filter=@x-tapper/sync-server build
RUN pnpm deploy --filter=@x-tapper/sync-server --prod /prod/@x-tapper/sync-server

FROM base AS sync-server
COPY --from=build /prod/@x-tapper/sync-server /prod/@x-tapper/sync-server
WORKDIR /prod/@x-tapper/sync-server
ENV NODE_ENV production
ENV PORT=5601
CMD [ "pnpm", "start" ]
EXPOSE 5601