import app from "./app";

const start = async () => {
  try {
    await app;
    await app.listen({ host: app.config.HOST ?? "localhost", port: app.config.PORT });
  } catch (error) {
    app.log.fatal(error);
    throw error;
  }
};

start();
