/* eslint-disable @typescript-eslint/naming-convention */
const configSchema = {
  type: "object",
  required: ["PORT"],
  properties: {
    HOST: {
      type: "string",
      default: "0.0.0.0",
    },
    PORT: {
      type: "string",
      default: 3000,
    },
    LOG_LEVEL: {
      type: "string",
      enum: ["debug", "error", "fatal", "info", "silent", "trace", "warn"],
      default: "debug",
    },
    LOG_PINO_PRETTY: {
      type: "boolean",
      default: "false",
    },
    LOG_FILE: {
      type: "string",
    },
    DB_FILE: {
      type: "string",
    },
  },
};

export default configSchema;
