
/* eslint-disable @typescript-eslint/naming-convention */
import tweetSchemaBase from "@x-tapper/x-tractor/schemas/TweetLite.json";

const tweetSchema = {
  ...tweetSchemaBase,
  $id: "tweet",
  properties: {
    ...tweetSchemaBase.properties,
    last_update: {
      type: "number",
    },
    _deleted: {
      type: "boolean",
    },
  },
};

export default tweetSchema;
