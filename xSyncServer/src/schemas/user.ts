
/* eslint-disable @typescript-eslint/naming-convention */
import userSchemaBase from "@x-tapper/x-tractor/schemas/UserLite.json";

const userSchema = {
  ...userSchemaBase,
  $id: "user",
  properties: {
    ...userSchemaBase.properties,
    last_update: {
      type: "number",
    },
    _deleted: {
      type: "boolean",
    },
  },
};

export default userSchema;
