/* eslint-disable unicorn/filename-case */
const generateSyncRequestBodySchema = (items: Record<string, unknown>) => ({
  type: "array",
  items: {
    type: "object",
    properties: {
      newDocumentState: {
        ...items,
      },
    },
    required: [
      "newDocumentState",
    ],
  },
});

export default generateSyncRequestBodySchema;
