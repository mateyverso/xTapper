import path from "node:path";
import { drizzle } from "drizzle-orm/better-sqlite3";
import Database from "better-sqlite3";
import config from "../config";
import * as schema from "./schemas";

export const databaseFile = path.join("data", "db", config.DB_FILE ?? "sqlite.db");

const sqlite = new Database(databaseFile);
const database = drizzle(sqlite, { schema });

export default database;
