/* eslint-disable @typescript-eslint/naming-convention */
import {
  sqliteTable, text, integer, index, primaryKey,
} from "drizzle-orm/sqlite-core";
import { type TweetLite } from "@x-tapper/x-tractor/types/TweetLite";
import { type UserLite } from "@x-tapper/x-tractor/types/UserLite";
import { relations } from "drizzle-orm";

export const tweets = sqliteTable("tweets", {
  bookmark_count: integer("bookmark_count").notNull(),
  card: text("card", { mode: "json" }).$type<TweetLite["card"]>(),
  conversation_control: text("conversation_control", { mode: "json" }).$type<TweetLite["conversation_control"]>(),
  conversation_id_str: text("conversation_id_str").notNull(),
  created_at: text("created_at", { length: 24 }).notNull(),
  display_text_range: text("display_text_range", { mode: "json" }).$type<TweetLite["display_text_range"]>().notNull(),
  entities: text("entities", { mode: "json" }).$type<TweetLite["entities"]>(),
  favorite_count: integer("favorite_count").notNull(),
  full_text: text("full_text").notNull(),
  hashtags: text("hashtags").notNull(),
  in_reply_to_screen_name: text("in_reply_to_screen_name"),
  in_reply_to_status_id_str: text("in_reply_to_status_id_str"),
  in_reply_to_user_id_str: text("in_reply_to_user_id_str"),
  lang: text("lang").notNull(),
  media: text("media", { mode: "json" }).$type<TweetLite["media"]>(),
  place: text("place"),
  quote_count: integer("quote_count").notNull(),
  quoted_status_id_str: text("quoted_status_id_str"),
  quoted_status_permalink: text("quoted_status_permalink"),
  reply_count: integer("reply_count").notNull(),
  rest_id: text("rest_id", { length: 32 }).primaryKey(),
  retweet_count: integer("retweet_count").notNull(),
  retweeted_status_result: text("retweeted_status_result"),
  source: text("source").notNull(),
  urls: text("urls", { mode: "json" }).$type<TweetLite["urls"]>().notNull(),
  user_id: text("user_id").notNull(),
  user_mentions: text("user_mentions", { mode: "json" }).$type<TweetLite["user_mentions"]>().notNull(),
  user_screen_name: text("user_screen_name").notNull(),
  views: integer("views"),

  last_update: integer("last_update").notNull(),
  _deleted: integer("_deleted", { mode: "boolean" }),
}, (table) => ({
  userIdx: index("user_idx").on(table.user_screen_name),
}));

export const users = sqliteTable("users", {
  created_at: text("created_at", { length: 24 }).notNull(),
  description: text("description").notNull(),
  descriptionUrls: text("descriptionUrls", { mode: "json" }).$type<UserLite["descriptionUrls"]>(),
  favourites_count: integer("favourites_count").notNull(),
  followers_count: integer("followers_count").notNull(),
  friends_count: integer("friends_count").notNull(),
  is_blue_verified: integer("is_blue_verified", { mode: "boolean" }).notNull(),
  listed_count: integer("listed_count").notNull(),
  location: text("location").notNull(),
  media_count: integer("media_count").notNull(),
  name: text("name").notNull(),
  pinned_tweet_ids_str: text("pinned_tweet_ids_str", { mode: "json" }).$type<UserLite["pinned_tweet_ids_str"]>().notNull(),
  professional: text("professional", { mode: "json" }).$type<UserLite["professional"]>(),
  profile_banner_url: text("profile_banner_url"),
  profile_image_url_https: text("profile_image_url_https").notNull(),
  protected: integer("protected", { mode: "boolean" }),
  rest_id: text("rest_id", { length: 32 }).primaryKey(),
  screen_name: text("screen_name").notNull(),
  statuses_count: integer("statuses_count").notNull(),
  url: text("url"),
  verified: integer("verified", { mode: "boolean" }).notNull(),
  verified_type: text("verified_type"),

  last_update: integer("last_update").notNull(),
  _deleted: integer("_deleted", { mode: "boolean" }),
}, (table) => ({
  screenNameIdx: index("screen_name_idx").on(table.screen_name),
}));

export const usersBasic = [
  users.rest_id,
  users.screen_name,
  users.name,
  users.description,
  users.followers_count,
  users.statuses_count,
  users.favourites_count,
  users.media_count,
  users.created_at,
  users.is_blue_verified,
  users.location,
  users.profile_image_url_https,
];

export const tweetsBasic = [
  tweets.rest_id,
  tweets.user_screen_name,
  tweets.full_text,
  tweets.reply_count,
  tweets.retweet_count,
  tweets.favorite_count,
  tweets.bookmark_count,
  tweets.in_reply_to_screen_name,
  tweets.in_reply_to_status_id_str,
];

export const lists = sqliteTable("lists", {
  id: text("id", { length: 36 }).primaryKey(), // GUID,
  order: integer("order").notNull(),
  name: text("name").notNull(),
  description: text("description"),
  color: text("color"),
  last_update: integer("last_update").notNull(),
  _deleted: integer("_deleted", { mode: "boolean" }),
});

export const listsOnTweets = sqliteTable("lists_tweets", {
  list_id: text("list_id", { length: 36 }).notNull().references(() => lists.id),
  tweet_id: text("tweet_id", { length: 32 }).notNull().references(() => tweets.rest_id),
  last_update: integer("last_update").notNull(),
  _deleted: integer("_deleted", { mode: "boolean" }),
}, (table) => ({
  pk: primaryKey({ columns: [table.list_id, table.tweet_id] }),
}));

export const listsRelations = relations(lists, ({ many }) => ({
  tweets: many(listsOnTweets),
}));

export const tweetsRelations = relations(tweets, ({ many }) => ({
  lists: many(listsOnTweets),
}));

export const listsOnTweetsRelations = relations(listsOnTweets, ({ one }) => ({
  list: one(lists, {
    fields: [listsOnTweets.list_id],
    references: [lists.id],
  }),
  tweet: one(tweets, {
    fields: [listsOnTweets.tweet_id],
    references: [tweets.rest_id],
  }),
}));

export const tweet_favorites = sqliteTable("tweet_favorites", {
  tweet_id: text("tweet_id", { length: 32 }).notNull(),
  user_id: text("user_id", { length: 32 }).notNull(),
  last_update: integer("last_update").notNull(),
  _deleted: integer("_deleted", { mode: "boolean" }),
}, (table) => ({
  pk: primaryKey({ columns: [table.tweet_id, table.user_id] }),
}));

export const tweet_retweets = sqliteTable("tweet_retweets", {
  tweet_id: text("tweet_id", { length: 32 }).notNull(),
  user_id: text("user_id", { length: 32 }).notNull(),
  last_update: integer("last_update").notNull(),
  _deleted: integer("_deleted", { mode: "boolean" }),
}, (table) => ({
  pk: primaryKey({ columns: [table.tweet_id, table.user_id] }),
}));

export const user_followers = sqliteTable("user_followers", {
  user_id: text("user_id", { length: 32 }).notNull(),
  follower_id: text("follower_id", { length: 32 }).notNull(),
  last_update: integer("last_update").notNull(),
  _deleted: integer("_deleted", { mode: "boolean" }),
}, (table) => ({
  pk: primaryKey({ columns: [table.user_id, table.follower_id] }),
}));

export const user_followings = sqliteTable("user_followings", {
  user_id: text("user_id", { length: 32 }).notNull(),
  following_id: text("following_id", { length: 32 }).notNull(),
  last_update: integer("last_update").notNull(),
  _deleted: integer("_deleted", { mode: "boolean" }),
}, (table) => ({
  pk: primaryKey({ columns: [table.user_id, table.following_id] }),
}));

export const allSchemas = [users,
  tweets,
  lists,
  listsOnTweets,
  tweet_favorites,
  tweet_retweets,
  user_followers,
  user_followings];

export type TweetInsert = typeof tweets.$inferInsert;
export type TweetSelect = typeof tweets.$inferSelect;
export type UserInsert = typeof users.$inferInsert;
export type UserSelect = typeof users.$inferSelect;
export type ListsInsert = typeof lists.$inferInsert;
export type ListsSelect = typeof lists.$inferSelect;
export type ListsOnTweetsInsert = typeof listsOnTweets.$inferInsert;
export type ListsOnTweetsSelect = typeof listsOnTweets.$inferSelect;
export type TweetFavoriteInsert = typeof tweet_favorites.$inferInsert;
export type TweetFavoriteSelect = typeof tweet_favorites.$inferSelect;
export type TweetRetweetInsert = typeof tweet_retweets.$inferInsert;
export type TweetRetweetSelect = typeof tweet_retweets.$inferSelect;
export type UserFollowerInsert = typeof user_followers.$inferInsert;
export type UserFollowerSelect = typeof user_followers.$inferSelect;
export type UserFollowingInsert = typeof user_followings.$inferInsert;
export type UserFollowingSelect = typeof user_followings.$inferSelect;

export type EntityInsert = TweetInsert | UserInsert | ListsInsert | ListsOnTweetsInsert
| TweetFavoriteInsert | TweetRetweetInsert | UserFollowerInsert | UserFollowingInsert;
