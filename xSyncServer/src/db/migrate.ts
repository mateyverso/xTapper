import { migrate } from "drizzle-orm/better-sqlite3/migrator";
import database from "./index";

migrate(database, { migrationsFolder: "data/migrations" });
