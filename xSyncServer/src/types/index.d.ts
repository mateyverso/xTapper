/* eslint-disable @typescript-eslint/consistent-type-definitions */
import { type FastifyInstance as Fastify } from "fastify";

type AppConfig = {
  HOST: string;
  PORT: number;
  LOG_LEVEL: "debug" | "error" | "fatal" | "info" | "silent" | "trace" | "warn";
  LOG_PINO_PRETTY: boolean;
  LOG_FILE?: string;
  DB_FILE?: string;
};

declare module "fastify" {
  // TODO: Declaration merging not working?
  interface FastifyInstance extends Fastify {
    config: AppConfig;
  }
}

declare global {
  namespace NodeJS {
    interface ProcessEnv extends AppConfig {

    }
  }
}
