/* eslint-disable @typescript-eslint/naming-convention */
import type Database from "better-sqlite3";
import { type FastifyRequest, type FastifyInstance, type FastifyReply } from "fastify";
import {
  upsertTweetFavorite, upsertTweetRetweet, upsertUserFollower, upsertUserFollowing,
} from "../repository/relations";
import {
  type TweetFavoriteInsert,
  type EntityInsert,
  type ListsInsert, type ListsOnTweetsInsert, type TweetInsert, type UserInsert, type TweetRetweetInsert, type UserFollowingInsert, type UserFollowerInsert,
} from "../db/schemas";
import {
  getListTweets, getLists, upsertList, upsertListOnTweet,
} from "../repository/listsRepo";
import { upsertTweet } from "../repository/tweetsRepo";
import { upsertUser } from "../repository/usersRepo";
import generateSyncRequestBodySchema from "../schemas/syncRequestBody";
import tweetFavoriteSchema from "../schemas/tweet_favorite.json";
import tweetRetweetSchema from "../schemas/tweet_retweet.json";
import userFollowerSchema from "../schemas/user_follower.json";
import userFollowingSchema from "../schemas/user_following.json";
import { getDashboard } from "../repository/dashboard";

type SyncBody<T> = Array<{
  newDocumentState: T;
}>;

type NoConflicts = []; // eslint-disable-line @typescript-eslint/ban-types
type InternalError = {
  error: string;
};
type SyncReply = NoConflicts | InternalError;

const syncEntities = ["tweets",
  "users",
  "lists",
  "listToTweet",
  "tweet_favorites",
  "tweet_retweets",
  "user_followers",
  "user_followings"] as const;

function syncHandler(entity: typeof syncEntities[number]) {
  const callbacks: Record<typeof syncEntities[number], (entity: any) => Promise<Database.RunResult>> = {
    tweets: upsertTweet,
    users: upsertUser,
    lists: upsertList,
    listToTweet: upsertListOnTweet,
    tweet_favorites: upsertTweetFavorite,
    tweet_retweets: upsertTweetRetweet,
    user_followers: upsertUserFollower,
    user_followings: upsertUserFollowing,
  };
  return async function <T extends EntityInsert>(request: FastifyRequest<{ Body: SyncBody<T> }>, reply: FastifyReply) {
    const entities = request.body.map((x) => x.newDocumentState);
    request.log.info(`Upserting ${entities.length} ${entity}`);
    try {
      // TODO: bulkInsert? allSettled?
      await Promise.all(
        entities.map(async (x) => callbacks[entity](x)),
      );
    } catch (error) {
      request.log.error(error);
      return reply.status(500).send({
        error: (error as any)?.message ?? "Internal error", // eslint-disable-line @typescript-eslint/no-unsafe-assignment
      });
    }

    reply.send([]);
  };
}

async function routes(server: FastifyInstance/* , options: Record<string, unknown> */) {
  server.post<{ Body: SyncBody<TweetInsert>; Reply: SyncReply }>("/tweets", {
    schema: {
      body: generateSyncRequestBodySchema({ $ref: "tweet#" }),
    },
    handler: syncHandler("tweets"),
  });

  server.post<{ Body: SyncBody<UserInsert>; Reply: SyncReply }>("/users", {
    schema: {
      body: generateSyncRequestBodySchema({ $ref: "user#" }),
    },
    handler: syncHandler("users"),
  });

  server.post<{ Body: SyncBody<ListsInsert>; Reply: SyncReply }>("/lists", {
    schema: {
      body: generateSyncRequestBodySchema({ $ref: "list#" }),
    },
    handler: syncHandler("lists"),
  });

  server.post<{ Body: SyncBody<ListsOnTweetsInsert>; Reply: SyncReply }>("/list_tweets", {
    schema: {
      body: generateSyncRequestBodySchema({ $ref: "listToTweet#" }),
    },
    handler: syncHandler("listToTweet"),
  });

  server.post<{ Body: SyncBody<TweetFavoriteInsert>; Reply: SyncReply }>("/tweet_favorites", {
    schema: {
      body: generateSyncRequestBodySchema(tweetFavoriteSchema),
    },
    handler: syncHandler("tweet_favorites"),
  });

  server.post<{ Body: SyncBody<TweetRetweetInsert>; Reply: SyncReply }>("/tweet_retweets", {
    schema: {
      body: generateSyncRequestBodySchema(tweetRetweetSchema),
    },
    handler: syncHandler("tweet_retweets"),
  });

  server.post<{ Body: SyncBody<UserFollowingInsert>; Reply: SyncReply }>("/user_followings", {
    schema: {
      body: generateSyncRequestBodySchema(userFollowingSchema),
    },
    handler: syncHandler("user_followings"),
  });

  server.post<{ Body: SyncBody<UserFollowerInsert>; Reply: SyncReply }>("/user_followers", {
    schema: {
      body: generateSyncRequestBodySchema(userFollowerSchema),
    },
    handler: syncHandler("user_followers"),
  });

  type StatsReply = Record<string, unknown>;
  server.get<{ Reply: StatsReply }>("/stats", async (_request, reply) => {
    const result: StatsReply = await getDashboard();
    reply.send(result);
  });

  type ListsReply = unknown[];
  server.get<{ Reply: ListsReply }>("/lists", async (_request, reply) => {
    const result: ListsReply = await getLists();
    reply.send(result);
  });

  // Type ListReply = unknown[];
  // type Paging = { offset?: number; limit?: number };
  server.get<{ Reply: ListsReply; Params: { id: string } }>("/lists/:id/tweets", async (request, reply) => {
    const listId = request.params.id;

    const result: ListsReply = await getListTweets(listId);
    reply.send(result);
  });
}

export default routes;
