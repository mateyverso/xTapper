import envSchema from "env-schema";
import { type AppConfig } from "./types";
import configSchema from "./schemas/config";

const envFile = process.env.NODE_ENV ? `.env.${process.env.NODE_ENV}` : ".env";

// https://github.com/fastify/fastify-env/issues/124
const config = envSchema<AppConfig>({
  env: true,
  dotenv: { path: envFile },
  schema: configSchema,
});

export default config;
