import fastify from "fastify";
import cors from "@fastify/cors";
import fastifyEnv from "@fastify/env";
import tweetSchema from "./schemas/tweet";
import userSchema from "./schemas/user";
import listSchema from "./schemas/listSchema.json";
import listToTweetSchema from "./schemas/listToTweetSchema.json";
import configSchema from "./schemas/config";
import routes from "./routes";
import config from "./config";

const app = fastify({
  logger: {
    level: process.env.LOG_LEVEL,
    ...(config.LOG_FILE ? {
      file: config.LOG_FILE,
    } : {}),
    ...(config.LOG_PINO_PRETTY ? {
      transport: {
        target: "pino-pretty",
        options: {
          translateTime: "HH:MM:ss Z",
          ignore: "pid,hostname",
          destination: config.LOG_FILE ?? 1,
        },
      },
    } : {}),
  },
});

// TODO: should these registers be awaited? how to export app then? wrap in a function call?
app.register(fastifyEnv, {
  env: true,
  dotenv: { path: ".env" },
  schema: configSchema,
});
app.register(cors, {
  origin: true, // TODO: limit to the extension scope
});

app.addSchema(tweetSchema);
app.addSchema(userSchema);
app.addSchema(listSchema);
app.addSchema(listToTweetSchema);

app.register(routes);

export default app;

