import { count, getTableName } from "drizzle-orm";
import database from "../db";
import {
  allSchemas,
} from "../db/schemas";

export async function getDashboard() {
  const counts = await Promise.all(
    allSchemas.map((schema) => database.select({ value: count() }).from(schema)),
  );
  return Object.fromEntries(allSchemas.map((schema, index) => [getTableName(schema), counts[index]![0]!.value]));
}
