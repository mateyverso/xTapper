import { sql } from "drizzle-orm"; // eslint-disable-line unicorn/filename-case
import database from "../db";
import { type TweetInsert, tweets as tweetsTable } from "../db/schemas";

export async function upsertTweet(tweet: TweetInsert) {
  try {
    return await database.insert(tweetsTable)
      .values(tweet)
      .onConflictDoUpdate({
        target: tweetsTable.rest_id,
        set: tweet,
        where: sql`${tweetsTable.last_update} < ${tweet.last_update}`,
      });
  } catch (error) {
    console.error("error upserting tweet", tweet.rest_id);
    console.dir(error);
    throw error;
  }
}
