import database from "../db";
import {
  type TweetFavoriteInsert, tweet_favorites, type TweetRetweetInsert, tweet_retweets, type UserFollowerInsert, user_followers, type UserFollowingInsert, user_followings,
} from "../db/schemas";

export async function upsertTweetFavorite(tweetFavorite: TweetFavoriteInsert) {
  return database.insert(tweet_favorites)
    .values(tweetFavorite)
    .onConflictDoNothing();
}

export async function upsertTweetRetweet(tweetRetweet: TweetRetweetInsert) {
  return database.insert(tweet_retweets)
    .values(tweetRetweet)
    .onConflictDoNothing();
}

export async function upsertUserFollower(userFollower: UserFollowerInsert) {
  return database.insert(user_followers)
    .values(userFollower)
    .onConflictDoNothing();
}

export async function upsertUserFollowing(userFollowing: UserFollowingInsert) {
  return database.insert(user_followings)
    .values(userFollowing)
    .onConflictDoNothing();
}
