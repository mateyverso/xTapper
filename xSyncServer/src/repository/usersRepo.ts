import { sql } from "drizzle-orm"; // eslint-disable-line unicorn/filename-case
import database from "../db";
import { type UserInsert, users as usersTable } from "../db/schemas";

export async function upsertUser(user: UserInsert) {
  try {
    return await database.insert(usersTable)
      .values(user)
      .onConflictDoUpdate({
        target: usersTable.rest_id,
        set: user,
        where: sql`${usersTable.last_update} < ${user.last_update}`,
      });
  } catch (error) {
    console.dir(error);
    console.error("--------", user);
    throw error;
  }
}
