import { desc, sql } from "drizzle-orm"; // eslint-disable-line unicorn/filename-case
import database from "../db";
import {
  type ListsInsert, lists as listsTable, type ListsOnTweetsInsert, listsOnTweets as listsOnTweetsTable, tweetsBasic,
} from "../db/schemas";

export async function upsertList(list: ListsInsert) {
  try {
    return await database.insert(listsTable)
      .values(list)
      .onConflictDoUpdate({
        target: listsTable.id,
        set: list,
        where: sql`${listsTable.last_update} < ${list.last_update}`,
      });
  } catch (error) {
    console.dir(error);
    console.error("--------", list);
    throw error;
  }
}

export async function upsertListOnTweet(listOnTweet: ListsOnTweetsInsert) {
  try {
    const ins = database.insert(listsOnTweetsTable)
      .values(listOnTweet)
      .onConflictDoUpdate({
        target: [listsOnTweetsTable.tweet_id, listsOnTweetsTable.list_id],
        set: listOnTweet,
        where: sql`${listsOnTweetsTable.last_update} < ${listOnTweet.last_update}`,
      });
    return await ins;
  } catch (error) {
    console.dir(error);
    console.error("--------", listOnTweet);
    throw error;
  }
}

export async function getLists() {
  return database.query.lists.findMany();
}

export async function getListTweets(listId: ListsOnTweetsInsert["list_id"], offset = 0, limit = 20) {
  const q = database.query.listsOnTweets.findMany({
    offset,
    limit,
    orderBy: [desc(listsOnTweetsTable.last_update)],
    where: (listOnTweets, { eq, and }) => and(
      eq(listOnTweets._deleted, false),
      eq(listOnTweets.list_id, listId),
    ),
    with: {
      tweet: {
        columns: Object.fromEntries(tweetsBasic.map((column) => [column.name, true])),
      },
    },
  });
  return q.execute();
}
