CREATE TABLE `lists` (
	`id` text(36) PRIMARY KEY NOT NULL,
	`order` integer NOT NULL,
	`name` text NOT NULL,
	`description` text,
	`color` text,
	`last_update` integer NOT NULL,
	`_deleted` integer
);
--> statement-breakpoint
CREATE TABLE `lists_tweets` (
	`list_id` text(36) NOT NULL,
	`tweet_id` text(32) NOT NULL,
	`last_update` integer NOT NULL,
	`_deleted` integer,
	PRIMARY KEY(`list_id`, `tweet_id`),
	FOREIGN KEY (`list_id`) REFERENCES `lists`(`id`) ON UPDATE no action ON DELETE no action,
	FOREIGN KEY (`tweet_id`) REFERENCES `tweets`(`rest_id`) ON UPDATE no action ON DELETE no action
);
--> statement-breakpoint
CREATE TABLE `tweets` (
	`bookmark_count` integer NOT NULL,
	`card` text,
	`conversation_control` text,
	`conversation_id_str` text NOT NULL,
	`created_at` text(24) NOT NULL,
	`display_text_range` text NOT NULL,
	`entities` text,
	`favorite_count` integer NOT NULL,
	`full_text` text NOT NULL,
	`hashtags` text NOT NULL,
	`in_reply_to_screen_name` text,
	`in_reply_to_status_id_str` text,
	`in_reply_to_user_id_str` text,
	`lang` text NOT NULL,
	`media` text,
	`place` text,
	`quote_count` integer NOT NULL,
	`quoted_status_id_str` text,
	`quoted_status_permalink` text,
	`reply_count` integer NOT NULL,
	`rest_id` text(32) PRIMARY KEY NOT NULL,
	`retweet_count` integer NOT NULL,
	`retweeted_status_result` text,
	`source` text NOT NULL,
	`urls` text NOT NULL,
	`user_id` text NOT NULL,
	`user_mentions` text NOT NULL,
	`user_screen_name` text NOT NULL,
	`views` integer,
	`last_update` integer NOT NULL,
	`_deleted` integer
);
--> statement-breakpoint
CREATE TABLE `users` (
	`created_at` text(24) NOT NULL,
	`description` text NOT NULL,
	`descriptionUrls` text,
	`favourites_count` integer NOT NULL,
	`followers_count` integer NOT NULL,
	`friends_count` integer NOT NULL,
	`is_blue_verified` integer NOT NULL,
	`listed_count` integer NOT NULL,
	`location` text NOT NULL,
	`media_count` integer NOT NULL,
	`name` text NOT NULL,
	`pinned_tweet_ids_str` text NOT NULL,
	`professional` text,
	`profile_banner_url` text,
	`profile_image_url_https` text NOT NULL,
	`protected` integer,
	`rest_id` text(32) PRIMARY KEY NOT NULL,
	`screen_name` text NOT NULL,
	`statuses_count` integer NOT NULL,
	`url` text,
	`verified` integer NOT NULL,
	`verified_type` text,
	`last_update` integer NOT NULL,
	`_deleted` integer
);
--> statement-breakpoint
CREATE INDEX `user_idx` ON `tweets` (`user_screen_name`);--> statement-breakpoint
CREATE INDEX `screen_name_idx` ON `users` (`screen_name`);