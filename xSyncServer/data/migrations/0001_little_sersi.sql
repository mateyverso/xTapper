CREATE TABLE `tweet_favorites` (
	`tweet_id` text(32) NOT NULL,
	`user_id` text(32) NOT NULL,
	`last_update` integer NOT NULL,
	`_deleted` integer,
	PRIMARY KEY(`tweet_id`, `user_id`)
);
--> statement-breakpoint
CREATE TABLE `tweet_retweets` (
	`tweet_id` text(32) NOT NULL,
	`user_id` text(32) NOT NULL,
	`last_update` integer NOT NULL,
	`_deleted` integer,
	PRIMARY KEY(`tweet_id`, `user_id`)
);
--> statement-breakpoint
CREATE TABLE `user_followers` (
	`user_id` text(32) NOT NULL,
	`follower_id` text(32) NOT NULL,
	`last_update` integer NOT NULL,
	`_deleted` integer,
	PRIMARY KEY(`follower_id`, `user_id`)
);
--> statement-breakpoint
CREATE TABLE `user_followings` (
	`user_id` text(32) NOT NULL,
	`following_id` text(32) NOT NULL,
	`last_update` integer NOT NULL,
	`_deleted` integer,
	PRIMARY KEY(`following_id`, `user_id`)
);
