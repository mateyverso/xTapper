import type { Config } from "drizzle-kit";

export default {
  schema: "src/db/schemas.ts",
  out: "./data/migrations",
  driver: "better-sqlite",
  dbCredentials: {
    url: "./data/sqlite.db",
  },
} satisfies Config;
