
import t from "tap";
import app from "../src/app";
import payload1 from "./tweets/1.json";

t.before(async () => {
  await import("../src/db/migrate");
});

t.test("POST /tweets", async (t) => {
  const a = await app;

  const response = await a.inject({
    method: "POST",
    url: "/tweets",
    body: payload1,
  });

  t.equal(response.statusCode, 200, "returns a status code of 200");
});
