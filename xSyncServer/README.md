# xSyncServer

API that receives data from the x-tapper extension and saves it to sqlite.

## Usage

### Node

```bash
# config
cp .env.sample .env
$EDITOR .env # edit the variables
# Alternatively use env vars

pnpm install

# generate empty db / run db migrations after updates
pnpm run migrate:run

# run in development mode
pnpm run dev

# run in production
pnpm run build
pnpm run start
```

### podman/docker:

```bash
# generate empty db / run db migrations after updates
podman run -v ~/x-tapper/xSyncServer/data/db:/prod/@x-tapper/sync-server/data/db -p 5601:5601 docker.io/mateyverso/x-tapper-sync-server:latest pnpm run migrate:run

# start the server
podman run -v ~/x-tapper/xSyncServer/data/db:/prod/@x-tapper/sync-server/data/db -p 5601:5601 docker.io/mateyverso/x-tapper-sync-server:latest
```

Alternatively:

```bash
# build container locally (from root dir)
podman build -t xtapper-sync-server:latest --target sync-server .

# generate empty db / run db migrations after updates
podman run -v ~/x-tapper/xSyncServer/data/db:/prod/@x-tapper/sync-server/data/db -p 5601:5601 sync-server:latest pnpm run migrate:run

# start the server
podman run -v ~/x-tapper/xSyncServer/data/db:/prod/@x-tapper/sync-server/data/db -p 5601:5601 sync-server:latest
```

## Ideas

- include authentication
- swagger docs

## initial generation

Based on
- https://fastify.dev/docs/latest/Reference/TypeScript/
- https://github.com/Looskie/fastify-drizzle-quick-start/

Tried to use xo with its [vscode extension](https://github.com/xojs/vscode-linter-xo) but was having problems with monorepo / [multi-root workspaces](https://code.visualstudio.com/docs/editor/multi-root-workspaces#_adding-folders);
So workspace settings are at root level.
`import/no-extraneous-dependencies: "off"` because the vscode plugin seems to use the monorepo root as the src root for the package.
May consider dropping xo in favour of a single eslint config for every repo.