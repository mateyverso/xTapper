export type Tweet = import('./types/Tweet.js').Tweet;
export type User = import('./types/User.js').User;
export type TweetLite = import('./types/TweetLite.js').TweetLite;
export type UserLite = import('./types/UserLite.js').UserLite;

export default function extract(content: object): {
    tweets: Tweet[];
    users: User[];
};

export function extractLite(content: object) : { tweets: TweetLite[], users: UserLite[] }

export function extractUsersTimeline(content: object) : UserLite[]

export function simplifyTweet(tweet: Tweet): TweetLite
export function simplifyUser(user: User): UserLite

export const supportedEndpoints: string[];
export const usersEndpoints: string[];

