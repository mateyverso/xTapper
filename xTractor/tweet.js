/**
 * xTractor extracts X posts and users from GraphQL responses
 * @module @x-tapper/x-tractor
 */

/**
 * @typedef {import('./types/Tweet.js').Tweet} Tweet
 * @typedef {import('./types/TweetLite.js').TweetLite} TweetLite
 * @typedef {import('./types/User.js').User} User
 * @typedef {import('./types/UserLite.js').UserLite} UserLite
 */

import get from 'lodash/get.js';
import set from 'lodash/set.js';
import unset from 'lodash/unset.js';
import omit from 'lodash/omit.js';
import pick from 'lodash/pick.js';

function extractSource(source) {
  const map = {
    "http://twitter.com/download/android": 'Android',
    "http://twitter.com/download/iphone": 'iPhone',
    "https://mobile.twitter.com": 'Mobile',
    "http://twitter.com": "Web",
    "https://buffer.com": "Buffer",
    "https://ifttt.com": 'IFTTT',
  };
  for (let src of Object.entries(map)) {
    if (source.includes(src[0])) {
      return src[1];
    }
  }
  return source;
}

function extractCard(card) {
  const values = card.legacy.binding_values;
  const getValue = (name, path = 'value.string_value') => get(values.find(v => v.key === name), path);

  // not returning images since I cannot GET them (404)
  const result = {
    url: getValue('card_url') || getValue('player_url'),
    title: getValue('title'),
    description: getValue('description'),
    domain: getValue('domain'),
    user_refs: card.legacy.user_refs_results,
  };
  Object.keys(result).forEach(key => result[key] === undefined ? delete result[key] : {});
  return result;
}

function extractPlace(place) {
  // coords available in bounding_box
  return place.full_name;
}

function extractEntities(entities) {
  const hashtags = entities.hashtags.map(h => h.text).join(',');
  const urls = entities.urls.map(url => url.expanded_url);
  const user_mentions = entities.user_mentions.map(um => pick(um, ['id_str', 'screen_name']));

  return { hashtags, urls, user_mentions };
}

function extractMedia(media) {
  return media.map(m => pick(m, ['media_url_https', 'type', 'expanded_url', 'media_key']))
}

function minimalTweet(tweet) {
  const baseFields = [
    'rest_id',
    'created_at',
    'user_id',
    'user_screen_name',
    'full_text',
    'display_text_range',
    'lang',
    'views',
    'quote_count',
    'reply_count',
    'retweet_count',
    'favorite_count',
    'bookmark_count',

    'conversation_control',
    'in_reply_to_screen_name',
    'in_reply_to_status_id_str',
    'in_reply_to_user_id_str',
    'conversation_id_str', // can be rest_id or in_reply_to_status_id_str or root tweet id.
    'quoted_status_id_str',

    'retweeted_status_result',
    
    
  ];
  // const omittedFields = [
    //     'id_str', 'user_id_str', 
    //     'previous_counts', // what is this? previous to being included in a quote?
    //     'is_quote_status', // redundant, check quoted_status_id_str / quoted_status_permalink
    //     'is_translatable', // ?
    //     'possibly_sensitive',
    //     'possibly_sensitive_editable',
    //     'limited_actions', // limited for logged in user only
    //     'unified_card', // ?? 
    // transfomed:
    //     'source', 'card', 'entities', 'extended_entities', 'place', 'note_tweet', 'quoted_status_permalink',
    // 'quoted_status_result', // removed because id already is in "quoted_status_id_str". the id after unwrap, but why sometimes undefiend? 1764741480088748437 ? rt?
  // ];

  const result = pick(tweet, baseFields);

  result.source = extractSource(tweet.source);
  if (tweet.card) {
    result.card = extractCard(tweet.card);
  }
  Object.assign(result, extractEntities(tweet.entities));
  if (tweet.extended_entities?.media) {
    result.media = extractMedia(tweet.extended_entities.media);
  }
  if (tweet.place) {
    result.place = extractPlace(tweet.place);
  }

  if (tweet.quoted_status_permalink) {
    result.quoted_status_permalink = get(tweet, 'quoted_status_permalink.expanded');
  }

  if (tweet.note_tweet) {
    result.full_text = get(tweet.note_tweet, 'note_tweet_results.result.text');
    result.entities = extractEntities(get(tweet.note_tweet, 'note_tweet_results.result.entity_set'))
  }
  return result;
}

//---------------------------------
/**
 * 
 * @param {Tweet} tweet 
 * @returns {TweetLite}
 */
export function simplifyTweet(tweet) {
  let result = { ...tweet, ...tweet.legacy };
  const omittedFields = [
    'has_birdwatch_notes',
    'unmention_data',
    'edit_control',
    'quick_promote_eligibility',
    'bookmarked',
    'favorited',
    'retweeted',
    'core',
    'legacy',
    '__typename',
    'superFollowsReplyUserResult',
  ];
  if (result.entities?.media && result.entities.media.length) {
    result.entities.media = result.entities.media.map(m => {
      return {
        ...omit(m, 'features', 'sizes'),
        original_info: omit(m.original_info, 'focus_rects')
      };
    });
  }
  if (result.extended_entities?.media && result.extended_entities.media.length) {
    result.extended_entities.media = result.extended_entities.media.map(m => {
      return {
        ...omit(m, 'features', 'sizes'),
        original_info: omit(m.original_info, 'focus_rects')
      };
    });
  }
  if (result.conversation_control) {
    result.conversation_control = {
      policy: get(result, 'conversation_control.policy'),
      screen_name: get(result, 'conversation_control.conversation_owner_results.result.legacy.screen_name', ''),
    };
  }
  if (result.quotedRefResult) {
    if (!result.quoted_status_result) {
      result.quoted_status_result = get(result.quotedRefResult, 'result.rest_id');
      unset(result, 'quotedRefResult');
    } else {
      console.warn('quoted_status_result set not expected')
      result.quotedRefResult = get(result.quotedRefResult, 'result.rest_id')
    }
  }
  result.views = result.views.count ? parseInt(result.views.count) : null;
  result.created_at = (new Date(result.created_at)).toISOString();
  return minimalTweet(omit(result, omittedFields))
}

export function extractFromVisibility(pseudoTweet) {
  return pseudoTweet.tweet ? pseudoTweet.tweet : pseudoTweet;
}

function isValidEntryId(entryId) {
  return entryId.startsWith('conversationthread') ||
    entryId.startsWith('tweet') ||
    entryId.startsWith('home-conversation');
}

/**
 * returns RT, Quoted tweet, or null
 * if any RT or quote, replaces tweetObj in-place with the id
 * @param {*} tweetObj
 * @returns {*} array containing the quote or RT and the tweet modified 
 * (id instead of full object in quote or rt props)
 */
export function unwrapQuoteAndRtFromTweet(tweetObj) {
  let rt = get(tweetObj, 'legacy.retweeted_status_result.result');
  const qt = get(tweetObj, 'legacy.quoted_status_result.result');
  const qtPath2 = get(tweetObj, 'quoted_status_result.result');
  if (rt && (qt || qtPath2)) {
    console.warn('UNHANDLED: rt & qt simultaneously');
    return [];
  } else if (rt) {
    if (rt.rest_id) {
      set(tweetObj, 'legacy.retweeted_status_result', rt.rest_id);
    } else if (rt.tweet) {
      set(tweetObj, 'legacy.retweeted_status_result', rt.tweet.rest_id);
      rt = rt.tweet;
    }
    const rt2 = get(rt, 'retweeted_status_result.result')
    const qt2 = get(rt, 'quoted_status_result.result')
    if (rt2) {
      console.warn('UNHANDLED: rt inside rt');
    }
    if (qt2) {
      set(rt, 'quoted_status_result', qt2.rest_id);
      return [qt2, rt, tweetObj];
    }
    return [rt, tweetObj];
  } else if (qt) {
    const rt2 = get(qt, 'retweeted_status_result.result')
    const qt2 = get(qt, 'quoted_status_result.result')
    if (rt2) console.warn('UNHANDLED: rt inside qt');
    if (qt2) console.warn('UNHANDLED: qt inside qt');
    set(tweetObj, 'legacy.quoted_status_result', qt.rest_id);
    return [qt, tweetObj];
  } else if (qtPath2) {
    const rt2 = get(qtPath2, 'retweeted_status_result.result')
    const qt2 = get(qtPath2, 'quoted_status_result.result')
    if (rt2) console.warn('UNHANDLED: rt inside qt');
    if (qt2) console.warn('UNHANDLED: qt inside qt');
    set(tweetObj, 'quoted_status_result', qtPath2.rest_id);
    return [qtPath2, tweetObj];
  }
  return [tweetObj];
}


/**
 * 
 * @param {*} entryObj 
 * @returns {Tweet[]}
 */
export function extractTweets(entryObj) {
  if (!entryObj) {
    // console.warn('no entryObj');
    return [];
  }
  if (!isValidEntryId(entryObj.entryId)) {
    // console.warn('invalid entry:', entryObj.entryId);
    return [];
  }
  const entryType = get(entryObj, 'content.entryType');
  if (entryType === 'TimelineTimelineModule') {
    const items = get(entryObj, 'content.items');
    if (!items) {
      console.warn('no items prop in TimeLineTimelineModule');
      return [];
    }
    const tweetItems = items
      .filter(i => isValidEntryId(i.entryId) && i.entryId.includes('-tweet-'))
      .map(i => get(i, 'item.itemContent'))
      .filter(i => !i.promotedMetadata && i.itemType === 'TimelineTweet')
      .map(i => get(i, 'tweet_results.result'))
      .filter(i => !i.tombstone) // filter tweets not visible
      .map(extractFromVisibility);
    const withQuotesAndRTs = tweetItems.flatMap(unwrapQuoteAndRtFromTweet);
    return withQuotesAndRTs;
  } else if (entryType === 'TimelineTimelineItem') {
    const content = get(entryObj, 'content.itemContent');
    if (content.itemType !== 'TimelineTweet') {
      console.warn('unknown itemType', content.itemType);
      return [];
    }
    if (content.tombstone || get(content, 'tweet_results.result.tombstone')) {
      return [];
    }
    const tweet = get(entryObj, 'content.itemContent.tweet_results.result');
    const quotesAndRTs = unwrapQuoteAndRtFromTweet(extractFromVisibility(tweet));
    return quotesAndRTs;
  } else if (typeof entryType === 'undefined' && get(entryObj, 'item.itemContent')) {
    // TimelineAddToModule child
    // TODO: refactor this mess!
    const tweet = get(entryObj, 'item.itemContent.tweet_results.result');
    if (tweet) {
      const quotesAndRTs = unwrapQuoteAndRtFromTweet(extractFromVisibility(tweet));
      return quotesAndRTs;
    } else {
      console.warn('entryType undefined case not well handled');
      return [];
    }
  } else {
    console.warn('unknown entryType', entryType);
    console.dir(entryObj);
    return [];
  }
}
