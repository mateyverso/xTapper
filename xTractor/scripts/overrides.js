import fs  from 'node:fs/promises';
import path from 'node:path';
import { fileURLToPath } from 'node:url';

const __dirname = path.dirname(fileURLToPath(import.meta.url));

const OVERRIDES_PATH = path.resolve(__dirname, '../schemas_overrides');

function overrideObjectProps(original, override) {
  for (const [key, value] of Object.entries(override)) {
      original.properties[key] = value;
  }
}

async function overrideFile(file) {
  const overrideFilePath = path.join(OVERRIDES_PATH, path.basename(file));

  try {
    await fs.stat(overrideFilePath);
  } catch (error) {
    if (error.code === 'ENOENT'){
      return;
    }
    throw error;
  }
  // console.log(`Running manual override for ${path.basename(file)}`);

  try {
    const originalData = JSON.parse(await fs.readFile(file, 'utf8'));
    const overrideData = JSON.parse(await fs.readFile(overrideFilePath, 'utf8'));
    overrideObjectProps(originalData, overrideData);
    await fs.writeFile(file, JSON.stringify(originalData));
  } catch (error) {
    console.error("An error occurred during merge:", error);
  }
}

async function main() {
  const file = process.argv[2];
  if (!file) throw new Error('usage: overrides.js <filePath>');
  await overrideFile(file)
}

main();