#!/bin/env sh

responses_folder="response_samples_extracts"
schemas_folder="schemas"
types_folder="types"

prepare() {
    # TODO: investigate what is the standard for this
    python3 -m venv .env
    . ./.env/bin/activate
    pip install -r ./requirements.txt | grep -v 'already satisfied'
    pnpm install -s
}

generateJsonSchemas() {
    for folder in ./"$responses_folder"/*/; do
        type=$(basename "$folder")
        genson -i 2 -d None "$folder"*.json | node ./scripts/canonicalize.js > "./$schemas_folder/$type.json"
        node ./scripts/overrides.js "./$schemas_folder/$type.json"
        pnpm exec prettier --log-level error "./$schemas_folder/$type.json" --write
    done
}

generateTypescript() {
    for filepath in ./"$schemas_folder"/*.json; do
        file=$(basename "$filepath")
        filename=${file%.*}
        npx --yes json-schema-to-typescript \
            -i "$filepath" -o "./$types_folder/$filename".ts --no-additionalProperties
    done
}

prepare
generateJsonSchemas
generateTypescript