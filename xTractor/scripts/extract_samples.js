import fg from "fast-glob";
import fs from "node:fs/promises";
import path from "node:path";
import extract, { simplifyTweet, simplifyUser, supportedEndpoints } from "../index.js";
import { dirname } from 'node:path';
import { fileURLToPath } from 'node:url';
import { extractUsersTimeline, usersEndpoints } from "../user.js";

const __dirname = dirname(fileURLToPath(import.meta.url));

const getResultFilename = (i, name) => path.join(__dirname, '..', 'response_samples_extracts', name, `${i}.json`);

async function extractUserTimelines(){
    const glob = `../response_samples/{${usersEndpoints.join(',')}}/*.json`;
    const entries = await fg([path.join(__dirname, glob)], { dot: false, braceExpansion: true });
    let userResults = [];
    for (const entry of entries) {
        const content = JSON.parse(await fs.readFile(entry));
        const users = extractUsersTimeline(content);
        userResults = [...userResults, ...(users || [])];
    }
    return userResults;
}

async function main(){
    const glob = `../response_samples/{${supportedEndpoints.join(',')}}/*.json`;
    const entries = await fg([path.join(__dirname, glob)], { dot: false, braceExpansion: true });
    let tweetResults = [];
    let userResults = [];
    for (const entry of entries) {
        const content = JSON.parse(await fs.readFile(entry));
        const { tweets, users } = extract(content);
        tweetResults = [...tweetResults, ...(tweets || [])];
        userResults = [...userResults, ...(users || [])];
    }
    for (let i = 0; i < tweetResults.length; i++) {
        const element = tweetResults[i];
        await fs.writeFile(getResultFilename(i, 'Tweet'), JSON.stringify(element, null, 4), { encoding: 'utf-8' });
    }
    for (let i = 0; i < userResults.length; i++) {
        const element = userResults[i];
        await fs.writeFile(getResultFilename(i, 'User'), JSON.stringify(element, null, 4), { encoding: 'utf-8' });
    }
    for (let i = 0; i < tweetResults.length; i++) {
        const element = simplifyTweet(tweetResults[i]);
        await fs.writeFile(getResultFilename(i, 'TweetLite'), JSON.stringify(element, null, 4), { encoding: 'utf-8' });
    }
    // appending here since I'm not getting the raw user, just the simplified
    userResults = [...userResults, ...await extractUserTimelines()];
    for (let i = 0; i < userResults.length; i++) {
        const element = simplifyUser(userResults[i]);
        await fs.writeFile(getResultFilename(i, 'UserLite'), JSON.stringify(element, null, 4), { encoding: 'utf-8' });
    }
}

main();

