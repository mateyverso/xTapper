#!/usr/bin/env node

import json from '@tufjs/canonical-json';

var stdin = process.stdin,
    stdout = process.stdout,
    inputChunks = [];

stdin.setEncoding('utf8');

stdin.on('data', function (chunk) {
    inputChunks.push(chunk);
});

stdin.on('end', function () {
    var inputJSON = inputChunks.join(''),
        parsedData = JSON.parse(inputJSON),
        outputJSON = json.canonicalize(parsedData);
    stdout.write(outputJSON);
    stdout.write('\n');
    process.exit(0);
});