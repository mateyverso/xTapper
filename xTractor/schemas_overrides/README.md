Include here top-level properties overrides. These will be included in the resulting schema in the "schemas" folder. Useful for manually overriding dynamic schema generation.
The keys in these objects will replace or add the keys provided to the corresponding schema "properties" (not merged).

Useful for patching validation errors found in production when no Twitter GraphQL example can be traced back.