# xTractor

Extract tweets and users from twitter GraphQL responses.

A single tweet sometimes contains information about related tweets and users. Those are unwrapped and put into separate entries, leaving id references only.

`index.js` provides utility functions for data extraction.
`types` contains TypeScript types and `schemas` JSON Schemas.

`User` and `Tweet` types are provided. Reduced version of these called `UserLite` and `TweetLite` are available, with the intention to discard private, irrelevant, or lenghty uninteresting fields.

## Usage

```js
import extract from "@x-tapper/x-tractor"

const jsonResponse = await getTwitterTimelineGraphQLResponse();
const { users, tweets } = extract(jsonResponse);
console.dir(users);
console.dir(tweets);
```

```ts
import { type TweetLite } from "@x-tapper/x-tractor/types/TweetLite";
import tweetSchema from "@x-tapper/x-tractor/schemas/TweetLite.json";
```

## Type re-generation

Intercept and save GraphQL responses into `./response_samples/{endpointName}/{sample number}.json`.

Response samples used to generate the initial types are not included and may be included in future releases.

Run `pnpm run start`.

It will:
 - From each response sample, extract tweet and user into individual json files in the folder `response_samples_extracts` (using this lib's extract methods).
 - Generate JSON Schemas using the available `response_samples_extracts`
 - Generate TS types using the JSON Schemas

The resulting types will be used by the library itself and available for consumers.

## Related projects

Some other projects I found interesting covering similar areas:

- https://github.com/fa0311/TwitterInternalAPIDocument
- https://github.com/fa0311/twitter-openapi-typescript/
- https://github.com/mackuba/bad_pigeon
