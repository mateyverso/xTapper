/**
 * xTractor extracts X posts and users from GraphQL responses
 * @module @x-tapper/x-tractor
 */

/**
 * @typedef {import('./types/Tweet.js').Tweet} Tweet
 * @typedef {import('./types/TweetLite.js').TweetLite} TweetLite
 * @typedef {import('./types/User.js').User} User
 * @typedef {import('./types/UserLite.js').UserLite} UserLite
 */

import get from 'lodash/get.js';
import { extractFromVisibility, extractTweets, simplifyTweet } from './tweet.js';
import { simplifyUser, unwrapUsersFromTweet } from './user.js';
export { simplifyTweet } from './tweet.js';
export { simplifyUser, usersEndpoints, extractUsersTimeline } from './user.js';

export const supportedEndpoints = ['HomeTimeline', 'HomeLatestTimeline', 'TweetDetail', 'SearchTimeline', 'UserByScreenName', 'UserTweets', 'Bookmarks'];

function getInstructions(responseObj){
    const instructionsPaths = {
        'HomeTimeline': 'data.home.home_timeline_urt.instructions',
        'HomeLatestTimeline': 'data.home.home_timeline_urt.instructions',
        'TweetDetail': 'data.threaded_conversation_with_injections_v2.instructions',
        'SearchTimeline': 'data.search_by_raw_query.search_timeline.timeline.instructions',
        'UserTweets': 'data.user.result.timeline_v2.timeline.instructions', 
        'Bookmarks': 'data.bookmark_timeline_v2.timeline.instructions',
    };

    return Object.values(instructionsPaths).reduce((a, b) => a || get(responseObj, b), null);
}

/**
 * 
 * @param {object} content 
 * @returns {{ tweets: Tweet[], users: User[] }}
 */
export default function extract(content){
    let tweets = []
    let users = [];
    const instructions = getInstructions(content);
    if (!instructions){
        if (get(content, 'data.user.result')){
            return {
                tweets: [],
                users: [get(content, 'data.user.result')]
            }
        }
        console.warn('no instructions nor user found in sample', content);
        return { tweets, users };
    }
    const entries = [
        ...(instructions.find(x => x.type === 'TimelineAddEntries')?.entries ?? []),
        ...(instructions.find(x => x.type === 'TimelineAddToModule')?.moduleItems ?? []), // when clicking view more (hidden response)
    ];
    const tweetsExtracted = entries.map(extractTweets).flat();
    tweets = [...tweets, ...tweetsExtracted.filter(x => !!x).map(extractFromVisibility)];

    for (let i = 0; i < tweets.length; i++) {
        const element = tweets[i];
        const tweetUsers = unwrapUsersFromTweet(element);
        users = [...users, ...tweetUsers];
    }

    return { tweets, users };
}

/**
 * 
 * @param {object} content 
 * @returns {{ tweets: TweetLite[], users: UserLite[] }}
 */
export function extractLite(content){
    const { users, tweets } = extract(content);
    console.log(users.length, users.map((user) => simplifyUser(user)).length);
    return {
        users: users.map((user) => simplifyUser(user)),
        tweets: tweets.map((tweet) => simplifyTweet(tweet))
    }
}