/**
 * xTractor extracts X posts and users from GraphQL responses
 * @module @x-tapper/x-tractor
 */

/**
 * @typedef {import('./types/Tweet.js').Tweet} Tweet
 * @typedef {import('./types/TweetLite.js').TweetLite} TweetLite
 * @typedef {import('./types/User.js').User} User
 * @typedef {import('./types/UserLite.js').UserLite} UserLite
 */

import get from 'lodash/get.js';
import set from 'lodash/set.js';
import unset from 'lodash/unset.js';
import pick from 'lodash/pick.js';


function extractUserEntities(entities) {
  return {
    descriptionUrls: get(entities, 'description.urls', []).map(u => u.expanded_url),
    url: get(entities, 'url.urls[0].expanded_url', ''),
  }
}

/**
* 
* @param {User} user 
* @returns {UserLite}
*/
export function simplifyUser(user) {
  let result = { ...user, ...user.legacy };
  const props = [
    "rest_id",
    "screen_name",
    "name",
    "description",
    "followers_count",
    "statuses_count",
    "favourites_count",
    "media_count",
    "created_at",
    "is_blue_verified",
    "location",
    "profile_image_url_https",
    "profile_banner_url",
    "friends_count",
    "listed_count",
    "pinned_tweet_ids_str",
    "professional",
    "verified",
    "verified_type",
    "protected",
  ];
  //   const omitted = [
  //     'can_dm', // private
  //     'can_media_tag', // ?
  //     'default_profile', // boolean. What is this?
  //     'default_profile_image', // infer by image urls?
  //     'fast_followers_count', // ?
  //     'followed_by', // private
  //     'following', // private
  //     'has_graduated_access', //
  //     'normal_followers_count', // ?
  //     'possibly_sensitive',
  //     'profile_image_shape',
  //     'profile_interstitial_type',
  //     'super_follow_eligible',
  //     'url', // already in entities, this is shortened link
  //     'want_retweets', // ?
  //     'withheld_in_countries',
  //     'entities', // extractUserEntities
  // ['id', 'affiliates_highlighted_label', 'has_custom_timelines', 'is_translator', 'translator_type', 'legacy', '__typename', 'muting']
  //   ];

  return {
    ...pick(result, props),
    ...extractUserEntities(result.entities)
  };
}

/**
 * @modifies {tweetObj}
 * @param {*} tweetObj 
 * @returns 
 */
export function unwrapUsersFromTweet(tweetObj) {
  const mainUser = get(tweetObj, 'core.user_results.result');
  if (!mainUser) {
    // console.warn('no user in tweet', tweetObj);
  } else {
    set(tweetObj, 'user_id', mainUser.rest_id)
    set(tweetObj, 'user_screen_name', get(mainUser, 'legacy.screen_name'));
    unset(tweetObj, 'core');
  }

  const cardUsers = get(tweetObj, 'card.legacy.user_refs_results');
  let cardUsersResult = [];
  if (cardUsers && Array.isArray(cardUsers)) {
    cardUsersResult = cardUsers.map(x => x.result).filter(x => !!x);
    set(tweetObj, 'card.legacy.user_refs_results', cardUsersResult.map(x => x.rest_id));
  }

  const medias = get(tweetObj, 'legacy.entities.media');
  const mediaUsers = [];
  if (medias && medias.length) {
    for (let i = 0; i < medias.length; i++) {
      const media = medias[i];
      const mediaUser = get(media, 'additional_media_info.source_user.user_results.result');
      if (mediaUser) {
        mediaUsers.push(mediaUser);
        set(media, 'additional_media_info.source_user', mediaUser.rest_id);
      }
    }
  }

  const extmedias = get(tweetObj, 'legacy.extended_entities.media');
  const extmediaUsers = [];
  if (extmedias && extmedias.length) {
    for (let i = 0; i < extmedias.length; i++) {
      const extmedia = extmedias[i];
      const extmediaUser = get(extmedia, 'additional_media_info.source_user.user_results.result');
      if (extmediaUser) {
        extmediaUsers.push(extmediaUser);
        set(extmedia, 'additional_media_info.source_user', extmediaUser.rest_id);
      }
    }
  }

  return [].concat(mainUser ? [mainUser] : [], cardUsersResult, mediaUsers, extmediaUsers);
}

export const usersEndpoints = ['Retweeters', 'Favoriters', 'Following', 'Followers'];

function getUsersInstructions(responseObj){
  const instructionsPaths = {
      'Retweeters': 'data.retweeters_timeline.timeline.instructions',
      'Favoriters': 'data.favoriters_timeline.timeline.instructions',
      'Following': 'data.user.result.timeline.timeline.instructions',
      'Followers': 'data.user.result.timeline.timeline.instructions',
  };
  return Object.values(instructionsPaths).reduce((a, b) => a || get(responseObj, b), null);
}

/**
 * 
 * @param {*} content 
 * @returns {UserLite[]}
 */
export function extractUsersTimeline(content) {
  let users = [];
  const instructions = getUsersInstructions(content);
  if (!instructions){
    console.warn('extractUsersTimeline: no instructions found', content);
    return users;
  }
  const instruction = instructions.find(i => i.type === 'TimelineAddEntries');
  const rawUsers = instruction.entries.filter(e => e.entryId.startsWith('user'))
    .map(entry => get(entry, 'content.itemContent.user_results.result'))
    .filter(entry => entry && entry.__typename === 'User');
  return rawUsers.map(u => simplifyUser(u));
}