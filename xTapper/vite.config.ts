import { resolve } from 'path'
import { defineConfig } from 'vite'
import preact from '@preact/preset-vite'
import cssInjectedByJsPlugin from 'vite-plugin-css-injected-by-js'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [preact(), cssInjectedByJsPlugin()],
  build: {
    rollupOptions: {
      input: {
        background: resolve(__dirname, './src/background/index.ts'),
        popup: resolve(__dirname, './src/popup/index.tsx'),
      },
      output: {
        entryFileNames: "scripts/[name].js"
      }
    },
    emptyOutDir: false,
  }
})
