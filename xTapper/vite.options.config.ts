import { resolve } from 'path'
import { defineConfig } from 'vite'
import preact from '@preact/preset-vite'
import cssInjectedByJsPlugin from 'vite-plugin-css-injected-by-js'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [preact(), cssInjectedByJsPlugin()],
  build: {
    rollupOptions: {
      input: {
        options: resolve(__dirname, './src/options/index.tsx'),
      },
      output: {
        entryFileNames: "scripts/[name].js"
      }
    },
    emptyOutDir: false,
  }
})
