export function saveCsv(content: string, filename: string){
    const blob = new Blob([content], { type: "text/csv;charset=utf-8;" });
    const url = window.URL.createObjectURL(blob);
    var pom = document.createElement('a');
    pom.href = url;
    pom.setAttribute('download', filename);
    pom.click();
}

export function t(msg: string, substitutions?: string | string[]){
    return browser.i18n.getMessage(msg, substitutions);
}