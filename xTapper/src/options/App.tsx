import { useEffect, useState } from "preact/hooks"
import Options from "./Options";
import { Lists } from "./Lists";
import { t } from "../utils";

export type Tab = 'options' | 'lists';
export function App() {
    const [tab, setTab] = useState<Tab>('options');

    useEffect(() => {
        function onHashChange(){
            if (location.hash === '#lists'){
                setTab('lists');
            } else {
                setTab('options');
            }
        }
        onHashChange();
        window.addEventListener('hashchange', onHashChange);

        return () => {
            window.removeEventListener('hashchange', onHashChange);
        }
    }, []);

    const View = tab === 'options' ? Options : Lists;

    return <div>
        <header>
            <h1>{t('extensionName')} {tab}</h1>
        </header>
        <nav>
            <a href="#options" class={tab === 'options' ? 'active' : ''}>{t('options')}</a>
            <a href="#lists" class={tab === 'lists' ? 'active' : ''}>{t('lists')}</a>
        </nav>
        <main>
            <View />
        </main>
    </div>
}