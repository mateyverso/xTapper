import { useEffect, useState } from "preact/hooks";
import { sendAction, useSendAction } from "../messaging";
import { List, Tweet } from "../types";
import TweetView from "./TweetView";
import { stringify } from 'csv-stringify/browser/esm';
import { saveCsv, t } from "../utils";

export default function ListView() {
    const [lists, setLists] = useState<List[]>([]);
    const [currentList, setCurrentList] = useState<List | null>(null);
    const [tweets, setTweets] = useState<Tweet[]>([]);

    useSendAction({ action: 'getLists' }, [], (response) => setLists(response));

    useEffect(() => {
        setTweets([]);
        if(!currentList){
            return;
        }
        sendAction({ action: 'getTweetsFromList', payload: {
            listId: currentList.id,
            limit: 50,
        }}).then(tweets => setTweets(tweets));
    }, [currentList]);

    async function exportList(){
        if(!currentList) return;
        const tweets = await sendAction({ action: 'getTweetsFromList', payload: {
            listId: currentList.id,
        }});
        stringify(tweets, { header: true }, (err, output) => {
            if (err) {
                return console.error("Error in csv generation", err);
            }
            const filename = `xTapper-list-${currentList.name}-${(new Date()).toISOString()}.csv`;
            saveCsv(output, filename);
        })
    }
    return <div>
        <div class="row">
            {
                lists.length ? 
                <select onChange={(e) => {
                    if(!e.currentTarget.value){
                        setCurrentList(null);
                    } else {
                        const toEdit = lists.find(l => l.name === e.currentTarget.value);
                        if (!toEdit) throw new Error(`list not found: ${e.currentTarget.value}`);
                        setCurrentList(toEdit);
                    }
                    }}>
                    <option></option>
                    {lists.map(l => (<option value={l.name}>{l.name}</option>))}
                </select> :
                <p>{t('empty_lists')}</p>
            }
            {
                currentList ? 
                <button class="primary" onClick={exportList}>{t('export_list')}</button> :
                null
            }
        </div>
        <div>
            {tweets.map(t => (<TweetView {...t} />))}
        </div>
    </div>
}