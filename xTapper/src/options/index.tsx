import { render } from 'preact';
import '../main.css';
import './options.css';
import { App } from './App';

render(<App />, document.getElementById('app')!);