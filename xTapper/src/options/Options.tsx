import { useState } from "preact/hooks";
import { sendAction, useSendAction } from "../messaging";
import { stringify } from 'csv-stringify/browser/esm';
import { saveCsv, t } from "../utils";
import useUpdateEffect from "../hooks";

export default function Options() {
    const [syncServerUrl, setSyncServerUrl] = useState("");
    const [syncServerStatus, setSyncServerStatus] = useState<null | boolean>(null);
    const [captureRtAndFav, setCaptureRtAndFav] = useState<boolean>(false);
    const [captureFollows, setCaptureFollows] = useState<boolean>(false);
    const [hideJunk, setHideJunk] = useState<boolean>(false);

    useSendAction({ action: 'getOptions' }, [], (options) => {
        setSyncServerUrl(options.syncServerUrl);
        setCaptureRtAndFav(options.captureRtAndFav);
        setCaptureFollows(options.captureFollows);
        setHideJunk(options.hideJunk);
    });

    useUpdateEffect(() => {
        sendAction({
            action: 'setOptions', payload: {
                captureRtAndFav, captureFollows, hideJunk
            }
        })
    }, [captureRtAndFav, captureFollows, hideJunk]);

    useSendAction({ action: 'testSyncServer', payload: syncServerUrl }, [syncServerUrl],
        async (response): Promise<boolean> => {
            setSyncServerStatus(response.syncServerOnline);
            if (response.syncServerOnline) {
                await sendAction({ action: 'setOptions', payload: { syncServerUrl } });
            }
            return response.syncServerOnline;
        }, (error) => {
            console.error("Error testing sync server", error);
            setSyncServerStatus(false);
            return false;
        });

    function onExport(collection: 'users' | 'tweets') {
        return async function () {
            const result = await sendAction({ action: 'getExport', payload: collection });
            const values = result.collections[0].docs;
            stringify(values, { header: true }, (err, output) => {
                if (err) {
                    return console.error("Error in csv generation", err);
                }
                const filename = `xTapper-${collection}-${(new Date()).toISOString()}.csv`;
                saveCsv(output, filename);
            })
        }
    }

    async function onPurge() {
        if (window.confirm(t('confirm_delete'))) {
            await sendAction({ action: 'purge' });
            window.alert(t('all_deleted'));
        }
    }

    return <>
        <div class="row sync-server">
            <label>
                {t('sync_server_url')}
                <input type="text" value={syncServerUrl} onInput={(e: InputEvent) => setSyncServerUrl((e.target as HTMLInputElement).value)} />
            </label>
            <div class="status">
                {syncServerStatus === true ? '🟢' : syncServerStatus === false ? '🔴' : null}
            </div>
        </div>
        <div class="row check">
            <input type="checkbox" name="captureRt" id="captureRt" checked={captureRtAndFav} onClick={() => setCaptureRtAndFav(!captureRtAndFav)} />
            <label for="captureRt">{t('capture_rt_and_favs')}</label>
        </div>
        <div class="row check">
            <input type="checkbox" name="captureFollows" id="captureFollows" checked={captureFollows} onClick={() => setCaptureFollows(!captureFollows)} />
            <label for="captureFollows">{t('capture_follows_followings')}</label>
        </div>
        <div class="row check">
            <input type="checkbox" name="hideJunk" id="hideJunk" checked={hideJunk} onClick={() => setHideJunk(!hideJunk)} />
            <label for="hideJunk">{t('hide_junk')}</label>
        </div>
        <div class="row">
            <label>{t('export_data')}</label>
            <button class="primary" onClick={onExport('tweets')}>{t('export_tweets')}</button>
            <button class="primary" onClick={onExport('users')}>{t('export_users')}</button>
        </div>
        <div class="row">
            <label>{t('purge_data')}</label>
            <button class="danger" onClick={onPurge}>{t('clear_local_db')}</button>
        </div>
    </>;
}