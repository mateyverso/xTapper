import { Tweet } from "../types";
import './TweetView.css';

export default function TweetView(props: Tweet){
    const text = props.full_text.slice(props.display_text_range[0], props.display_text_range[1]);
    const userUrl = `https://twitter.com/${props.user_screen_name}`;
    const date = new Date(props.created_at).toISOString();
    const permalink = `${userUrl}/status/${props.rest_id}`;
    return <div class="tweet-view">
        <div></div>
        <div class="tweet-container">
            <div class="tweet-metadata">
                <a href={userUrl} target="_blank">{props.user_screen_name}</a>
                <a href={permalink}>{date}</a>
            </div>
            <div class="tweet-text">{text}</div>
            <div class="tweet-media">
                {
                    props.media ? 
                    props.media.map(m => <a href={m.expanded_url} target="_blank" className={m.type === 'video' ? 'video': 'image'}>
                        <img src={m.media_url_https} />
                    </a>) :
                    null
                }
            </div>
        </div>
    </div>
}