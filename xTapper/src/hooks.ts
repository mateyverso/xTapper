import { useEffect, useRef } from "preact/hooks";

/**
 * A custom useEffect hook that only triggers on updates, not on initial mount
 * https://stackoverflow.com/questions/55075604/react-hooks-useeffect-only-on-update
 */
export default function useUpdateEffect(effect: Function, dependencies: Array<unknown> = []) {
    const isInitialMount = useRef(true);
  
    useEffect(() => {
      if (isInitialMount.current) {
        isInitialMount.current = false;
      } else {
        return effect();
      }
    }, dependencies);
  }
  