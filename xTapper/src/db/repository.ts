import { DeepReadonlyObject } from "rxdb";
import { List, Tweet, User } from "../types";
import getDb from './index';
import groupBy from "lodash/groupBy";
import mapValues from "lodash/mapValues";

export const addTweets = async function (tweets: Tweet[]) {
    const db = getDb();
    const result = await db.tweets.bulkUpsert(
        tweets.map(t => ({ ...t, last_update: Date.now() }))
    );
    return result;
}

export const addUsers = async function (users: User[]) {
    const db = getDb();
    const result = await db.users.bulkUpsert(
        users.map(u => ({ ...u, last_update: Date.now() }))
    );
    return result;
}

export const addTweet = async function (t: Tweet) {
    const db = getDb();
    try {
        const result = await db.tweets.upsert({ ...t, last_update: Date.now() });
        return result;
    } catch (error) {
        console.error(error);
        return null;
    }
}

export const addUser = async function (u: User) {
    const db = getDb();
    try {
        const result = await db.users.upsert({ ...u, last_update: Date.now() });
        return result;
    } catch (error) {
        console.error(error);
        return null;
    }
}

export async function addFollowers(userId: string, followers: User[]) {
    const db = getDb();
    await db.user_followers.bulkUpsert(followers.map(f => ({
        user_id: userId,
        follower_id: f.rest_id,
        last_update: Date.now()
    })));
}

export async function addFollowing(userId: string, followings: User[]) {
    const db = getDb();
    await db.user_followings.bulkUpsert(followings.map(f => ({
        user_id: userId,
        following_id: f.rest_id,
        last_update: Date.now()
    })));
}

export async function addRetweets(tweetId: string, users: User[]) {
    const db = getDb();
    await db.tweet_retweets.bulkUpsert(users.map(u => ({
        tweet_id: tweetId,
        user_id: u.rest_id,
        last_update: Date.now()
    })));
}

export async function addFavorites(tweetId: string, users: User[]) {
    const db = getDb();
    await db.tweet_favorites.bulkUpsert(users.map(u => ({
        tweet_id: tweetId,
        user_id: u.rest_id,
        last_update: Date.now()
    })));
}

export const getLocalStats = async function () {
    const db = getDb();
    try {
        const [tweets, users] = await Promise.all([
            db.tweets.count().exec(),
            db.users.count().exec()
        ]);
        console.log('local stats', tweets, users)
        return { tweets, users };
    } catch (error) {
        console.error('error getting local stats', error)
        return {
            tweets: 0,
            users: 0
        }
    }
}

export async function getList(id: string) {
    const db = getDb();
    return db.lists.findOne(id).exec();
}

export async function getAllLists() {
    const db = getDb();
    const lists = await db.lists.find({
        sort: [
            { order: 'asc' }
        ]
    }).exec();
    return lists.map(l => l.toJSON());
}

/**
 * @returns true if added, false if removed
 */
export async function addToList(listId: string, tweetId: string): Promise<boolean> {
    const db = getDb();
    const existing = await db.list_tweets.findOne({ selector: { list_id: listId, tweet_id: tweetId } }).exec();
    if (existing) {
        await existing.remove()
        return false;
    } else {
        await db.list_tweets.insert({ list_id: listId, tweet_id: tweetId, last_update: Date.now() })
        return true;
    }
}

export async function createList(list: List): Promise<DeepReadonlyObject<List>> {
    const db = getDb();
    return (await db.lists.insert({ ...list, last_update: Date.now() })).toJSON();
}

export async function updateList(id: string, data: List): Promise<DeepReadonlyObject<List>> {
    const db = getDb();
    const doc = await db.lists.findOne({ selector: { id } }).exec();
    if (!doc) {
        throw new Error(`list ${id} not found`);
    }
    await doc.patch({ ...data, last_update: Date.now() });
    return doc.toJSON();
}

export async function deleteList(id: string) {
    const list = await getList(id);
    if (!list) {
        console.warn(`list id ${id} not found`);
        return null;
    }
    return (await list.remove()).toJSON()
}

export async function getListsForTweets(tweetIds: string[]): Promise<Record<string, string[]>> {
    const db = getDb();
    const listedTweets = await db.list_tweets.find({
        selector: {
            tweet_id: { $in: tweetIds }
        },
    }).exec();
    const grouped = groupBy(listedTweets, 'tweet_id');
    return mapValues(grouped, (val) => val.map(list => list.list_id))
}

export async function getExport(collection: 'users' | 'tweets') {
    const db = getDb();
    return db.exportJSON([collection]);
}

export async function getTweetsFromList(listId: string, limit?: number, from?: number) {
    const db = getDb();
    const listedTWeets = await db.list_tweets.find({
        selector: {
            list_id: listId,
            ...(from ? { last_update: { $gt: from }} : {})
        },
        limit,
        sort: [{ last_update: 'desc' }]
    }).exec();
    const tweetIds = listedTWeets.map(t => t.tweet_id);
    const tweets = await db.tweets.find({
        selector: {
            rest_id: { $in: tweetIds }
        }
    }).exec();
    return tweets.map(t => t.toJSON())
        .sort((a, b) => tweetIds.indexOf(a.rest_id) - tweetIds.indexOf(b.rest_id) );
}

export async function purge() {
    const db = getDb();
    await db.tweets.remove();
    await db.users.remove();
}
