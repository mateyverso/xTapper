import userLiteSchema from '@x-tapper/x-tractor/schemas/UserLite.json';

const userSchema = {
    ...userLiteSchema,
    "version": 0,
    "primaryKey": "rest_id",
    "indexes": [
        "screen_name"
    ],
    "properties":{
        ...userLiteSchema.properties,
        "rest_id": {
            "type": "string",
            "maxLength": 32
        },
        "last_update": {
            "type": "number",
            "minimum": 0,
            "maximum": 2701307494132,
            "multipleOf": 1
        },
        "screen_name": {
            "type": "string",
            "maxLength": 32 // TODO: what's the max length?
        }
    }
}

export default userSchema;