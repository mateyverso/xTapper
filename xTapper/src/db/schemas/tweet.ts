import tweetLiteSchema from '@x-tapper/x-tractor/schemas/TweetLite.json';

const tweetSchema = {
    ...tweetLiteSchema,
    "version": 0,
    "primaryKey": "rest_id",
    "indexes": [
        "user_screen_name"
    ],
    "properties":{
        ...tweetLiteSchema.properties,
        "rest_id": {
            "type": "string",
            "maxLength": 32
        },
        "last_update": {
            "type": "number",
            "minimum": 0,
            "maximum": 2701307494132,
            "multipleOf": 1
        },
        "user_screen_name": {
            "type": "string",
            "maxLength": 32 // TODO: what's the max length?
        }
    }
}

export default tweetSchema;