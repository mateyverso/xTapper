import { RxCollection, RxDatabase, RxJsonSchema, addRxPlugin, createRxDatabase } from 'rxdb';
import { RxDBDevModePlugin } from 'rxdb/plugins/dev-mode';
import { RxDBJsonDumpPlugin } from 'rxdb/plugins/json-dump';
import { getRxStorageDexie } from 'rxdb/plugins/storage-dexie';
import tweetSchema from './schemas/tweet';
import userSchema from './schemas/user';
import tweetFavoriteSchema from './schemas/tweet_favorite.json';
import tweetRetweetSchema from './schemas/tweet_retweet.json';
import userFollowingSchema from './schemas/user_following.json';
import userFollowerSchema from './schemas/user_follower.json';
import listSchema from './schemas/list.json';
import listTweetSchema from './schemas/list_tweet.json';

import { Tweet, TweetFavorite, TweetRetweet, User, 
  UserFollower, UserFollowing, List, ListToTweet } from '../types';
addRxPlugin(RxDBDevModePlugin);
addRxPlugin(RxDBJsonDumpPlugin);

export type AuditFields = { last_update: number };
export type TweetDB = Tweet & AuditFields;
export type UserDB = User & AuditFields;

export interface Database {
  tweets: RxCollection<TweetDB, {}>,
  users: RxCollection<UserDB, {}>,
  tweet_favorites: RxCollection<TweetFavorite, {}>,
  tweet_retweets: RxCollection<TweetRetweet, {}>,
  user_followers: RxCollection<UserFollower, {}>,
  user_followings: RxCollection<UserFollowing, {}>,
  lists: RxCollection<List, {}>,
  list_tweets: RxCollection<ListToTweet, {}>,
}

const lastUpdate = (d: AuditFields) => {
  // TODO: Check if this is always triggering (field was not set)
  d.last_update = Date.now();
  return d;
};

let db: RxDatabase<Database> | null = null;

export async function createDb() {
  const database = await createRxDatabase<Database>({
      name: 'everything',
      storage: getRxStorageDexie()
    });
  
  await database.addCollections({
    tweets: {
      schema: tweetSchema as RxJsonSchema<TweetDB>
    },
    users: {
      schema: userSchema as RxJsonSchema<UserDB>
    },
    tweet_favorites: {
      schema: tweetFavoriteSchema as RxJsonSchema<TweetFavorite>
    },
    tweet_retweets: {
      schema: tweetRetweetSchema as RxJsonSchema<TweetRetweet>
    },
    user_followers: {
      schema: userFollowerSchema as RxJsonSchema<UserFollower>
    },
    user_followings: {
      schema: userFollowingSchema as RxJsonSchema<UserFollowing>
    },
    lists: {
      schema: listSchema as RxJsonSchema<List>
    },
    list_tweets: {
      schema: listTweetSchema as RxJsonSchema<ListToTweet>
    },
  });
  Object.keys(database.collections).forEach((collection: string) => {
    const collectionName = collection as keyof typeof database.collections;
    database.collections[collectionName].preSave(lastUpdate, true);
    database.collections[collectionName].preRemove(lastUpdate, true);
  });
  db = database;
  return database;
}

export default function getDb() { 
  if (!db) throw new Error('Trying to access db before initialization');
  return db 
};


