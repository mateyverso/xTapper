import { replicateRxCollection } from 'rxdb/plugins/replication';
import omit from 'lodash/omit';
import getDb, { type Database } from './index';
import state from '../background/state';

async function genericReplication(syncServerUrl: string, collection: keyof Database, omitFields: string[] = []){
    let failures = 0;
    const MAX_RETRY = 3;
    const db = getDb();
    const repl = replicateRxCollection({
        collection: db[collection],
        replicationIdentifier: `${collection}-replication`,
        retryTime: 3000,
        push: { 
          async handler(changeRows){
            const changes = changeRows.map(x => ({
              ...x,
              newDocumentState: omit(x.newDocumentState, omitFields)
            }));
            const rawResponse = await fetch(`${syncServerUrl}/${collection}`, {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(changes)
            });
            const conflictsArray = await rawResponse.json();
            return conflictsArray;
          }  
        },
    });
    repl.error$.subscribe({
      next(value) {
        if (value.parameters.errors && value.parameters.errors.length){
          const hasNetworkError = value.parameters.errors.some(e => e.message.includes('NetworkError'));
          if (hasNetworkError){
            failures++;
          }
        }
        if (failures >= MAX_RETRY){
          console.warn(`Replication failed ${failures} times. Suspending replication`);
          repl.cancel();
          state.isReplicating = false;
        }
      },
    })
    return repl;
  }
  
  
  export async function replicate(syncServerUrl: string){
    console.info(`Starting replication to ${syncServerUrl}`);
    state.isReplicating = true;
    const mainCollections: (keyof Database)[] = ['tweets', 'users','lists'];
    const collections: (keyof Database)[] = ['tweet_favorites', 'tweet_retweets', 'user_followers', 'user_followings', 'list_tweets']
    await Promise.all(mainCollections.map(c => genericReplication(syncServerUrl, c)));
    await Promise.all(collections.map(c => genericReplication(syncServerUrl, c)));
  }
  