import { addToList, createList, deleteList, getAllLists, getExport, getListsForTweets, getLocalStats, getTweetsFromList, purge, updateList } from '../db/repository';
import { Options, Stats } from '../types';
import { Action } from '../messaging';
import state from './state';
import { replicate } from '../db/replication';

export async function setRecording(isOn: boolean) {
    await browser.storage.local.set({ isRecording: isOn });
    state.isRecording = isOn;
    browser.action.setIcon({
        path: `xTapper2${isOn ? '-on' : ''}.svg`
    });
    return isOn;
}

export async function getStats(syncServerUrl: string): Promise<Stats> {
    const localStats = await getLocalStats();
    if (!syncServerUrl) {
        throw new Error('No sync server url provided.');
    }
    try {
        const response = await fetch(`${syncServerUrl}/stats`);
        const data: { tweets: number, users: number } = await response.json();
        if (response.status !== 200)
            throw new Error(`Error getting stats from ${syncServerUrl}: ${response.status} response`);
        if (!state.isReplicating){
            replicate(syncServerUrl);
        }
        return { ...localStats, syncServerOnline: true, syncServer: data };
    } catch (error) {
        console.error(`Error getting stats from ${syncServerUrl}:`, error);
        return { ...localStats, syncServerOnline: false }
    }
}

export async function getOptions(): Promise<Options> {
    const defaultOptions: Options = {
        syncServerUrl: 'http://localhost:5601',
        captureFollows: true,
        captureRtAndFav: true,
        hideJunk: true,
    }
    const options = await browser.storage.local.get(defaultOptions) as Options;
    return options;
}

export async function setOptions(options: Partial<Options>) {
    await browser.storage.local.set(options);
    Object.assign(state, options);
}

export default async function messageListener(action: Action) {
    console.log('Handle Action', action);
    switch (action.action) {
        case 'toggleRecording': return setRecording(action.payload);
        case 'getIsRecording': return state.isRecording;
        case 'getOptions': return getOptions();
        case 'setOptions': return setOptions(action.payload);
        case 'getStats': return getStats(state.syncServerUrl);
        case 'testSyncServer': return getStats(action.payload);
        case 'getExport': return getExport(action.payload);
        case 'purge': return purge();
        case 'getLists': return getAllLists();
        case 'createList': return createList(action.payload);
        case 'deleteList': return deleteList(action.payload.id);
        case 'selectTweet': return addToList(action.payload.list, action.payload.id);
        case 'updateList': return updateList(action.payload.id, action.payload.data);
        case 'getListsForTweets': return getListsForTweets(action.payload);
        case 'getTweetsFromList': return getTweetsFromList(action.payload.listId, action.payload.limit, action.payload.from);
        default:
            break;
    }
    return Promise.resolve(null);
}