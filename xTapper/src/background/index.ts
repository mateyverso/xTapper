import type { WebRequest } from 'webextension-polyfill';
import { createDb } from '../db';
import { replicate } from '../db/replication';
import messageListener, { getOptions, setRecording } from './messageListener';
import state from './state';
import routes from './interceptionRoutes';

let urls = [
    "https://twitter.com/i/api/graphql/*", 
    "https://x.com/i/api/graphql/*"
];

function filter(requestDetails: WebRequest.OnBeforeRequestDetailsType, endpoints: string[], callback: (o: object, requestDetails: WebRequest.OnBeforeRequestDetailsType) => void){
    if (endpoints.some(e => requestDetails.url.includes(e))){
        console.log('Intercept: ', requestDetails.url, requestDetails.requestId)
        let filter = browser.webRequest.filterResponseData(requestDetails.requestId);
        let decoder = new TextDecoder("utf-8");
        let responseData = '';
        filter.ondata = event => {
            // TODO: is this fine?
            responseData = responseData + decoder.decode(event.data, {stream: true});
            filter.write(event.data);
        }
        
        filter.onstop = () => {
            let response = null;
            try {
                response = JSON.parse(responseData);
            } catch (error) {
                console.warn('Invalid JSON', responseData);
            }
            if(response){
                callback(response, requestDetails);
            }
            filter.close();
        }
    }
}

function onBeforeRequestHandler(requestDetails: WebRequest.OnBeforeRequestDetailsType) {
    if (!state.isRecording) {
        return;
    }
    routes.forEach(([endpoints, callback]) => filter(requestDetails, endpoints, callback));
    return {};
}

async function loadOptions(){
    state.isRecording = (await browser.storage.local.get('isRecording')).isRecording;
    const options = await getOptions();
    Object.assign(state, options);
}

async function load(){
    try {
        await createDb();
    } catch (error) {
        console.error('Error on db init', error);
        throw error;
    }
    await loadOptions();    
    setRecording(state.isRecording);
    browser.webRequest.onBeforeRequest.addListener(onBeforeRequestHandler, { urls }, ['blocking']);
    browser.runtime.onMessage.addListener(messageListener);
    await replicate(state.syncServerUrl);
}

load();
  