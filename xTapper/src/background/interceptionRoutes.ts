import { WebRequest } from 'webextension-polyfill';
import { addFavorites, addFollowers, addFollowing, addRetweets, addTweets, addUsers } from '../db/repository';
import extract, { extractUsersTimeline, simplifyTweet, simplifyUser, supportedEndpoints, usersEndpoints } from '@x-tapper/x-tractor';
import state from './state';

async function general(obj: object) {
    try {
        const { tweets, users } = extract(obj);
        await Promise.all([
            addTweets(tweets.map(simplifyTweet)),
            addUsers(users.map(simplifyUser))
        ]);
    } catch (error) {
        console.error('Failed to Extract/Insert', obj, error);
    }
}

async function users(obj: object, request: WebRequest.OnBeforeRequestDetailsType) {
    const url = new URL(request.url);
    const endpoint = url.pathname.split('/').at(-1);
    const users = extractUsersTimeline(obj);
    await addUsers(users);

    const getReqVariable = (prop: string) => JSON.parse(url.searchParams.get('variables')!)[prop]

    switch (endpoint) {
        case 'Followers':
            if (!state.captureFollows) return;
            return await addFollowers(getReqVariable('userId'), users)
        case 'Following':
            if (!state.captureFollows) return;
            return await addFollowing(getReqVariable('userId'), users)
        case 'Retweeters':
            if (!state.captureRtAndFav) return;
            return await addRetweets(getReqVariable('tweetId'), users)
        case 'Favoriters':
            if (!state.captureRtAndFav) return;
            return await addFavorites(getReqVariable('tweetId'), users)
        default:
            console.warn('users endpoint not caught', url.toString());
            break;
    }
}

const routes: [string[], (o: object, requestDetails: WebRequest.OnBeforeRequestDetailsType) => void][] = [
    [supportedEndpoints, general],
    [usersEndpoints, users]
];

export default routes;