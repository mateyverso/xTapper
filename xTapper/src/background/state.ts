import { Options } from "../types";

export type BackgroundState = {
    isRecording:  boolean,
    isReplicating: boolean,
} & Options;

const state: BackgroundState = {
    isRecording: false,
    isReplicating: false,
    captureFollows: true,
    captureRtAndFav: true,
    hideJunk: true,
    syncServerUrl: '',
}

export default state;