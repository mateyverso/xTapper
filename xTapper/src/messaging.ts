import { useEffect } from "preact/hooks";
import { List, Options, Stats, Tweet } from "./types";
import { RxDumpDatabase } from "rxdb";
import { Database } from "./db";

export type Action = {
    action: 'toggleRecording',
    payload: boolean
} | {
    action: 'getIsRecording',
} | {
    action: 'getOptions',
} | {
    action: 'setOptions',
    payload: Partial<Options>
} | {
    action: 'getStats'
} | {
    action: 'testSyncServer',
    payload: string
} | {
    action: 'getExport',
    payload: 'users' | 'tweets'
} | {
    action: 'purge'
} | {
    action: 'getLists'
}| {
    action: 'selectTweet',
    payload: {
        id: string,
        list: string,
    }
}| {
    action: 'createList',
    payload: List,
}| {
    action: 'updateList',
    payload: {
        id: string,
        data: List,
    }
}| {
    action: 'deleteList',
    payload: {
        id: string,
    }
} | {
    action: 'noop',
} | {
    action: 'getListsForTweets',
    payload: string[]
} | {
    action: 'getTweetsFromList',
    payload: {
        listId: string,
        limit?: number,
        from?: number
    }
};

export type ActionResponse<T extends Action['action']> = 
    T extends 'toggleRecording'? boolean : 
    T extends 'getIsRecording'? boolean : 
    T extends 'getOptions'? Options : 
    T extends 'setOptions'? void : 
    T extends 'getStats'? Stats : 
    T extends 'testSyncServer'? Stats :
    T extends 'getExport'? RxDumpDatabase<Database> :
    T extends 'purge' ? void :
    T extends 'getLists' ? List[] :
    T extends 'selectTweet' ? boolean :
    T extends 'createList' ? void :
    T extends 'updateList' ? void :
    T extends 'deleteList' ? void :
    T extends 'noop' ? void : 
    T extends 'getListsForTweets' ? Record<string, string[]> :
    T extends 'getTweetsFromList' ? Tweet[] :
    never;

export async function sendAction<T extends Action>(action: T): Promise<ActionResponse<T['action']>> {
    const woke = await wakeUp();
    if (!woke){
        throw new Error('background not woke');
    }
    return await browser.runtime.sendMessage(action);
}

export function useSendAction<T extends Action>(action: T, deps: Array<unknown>, 
    callback: (response: ActionResponse<T['action']>) => unknown, 
    errorCallback?: (error: Error) => unknown): void {
        const defaultErrorCallback = (error: Error) => {
            console.log(`Error sending action ${action.action}:`, error);
        };
        useEffect(() => {
            sendAction(action)
                .then(callback)
                .catch(errorCallback ?? defaultErrorCallback);
        }, deps);
}

/**
 * The background script is not persistent. Sometimes wont be available
 * I'm using this to avoid using `browser.runtime.connect` ports
 */
export async function wakeUp(): Promise<boolean>{
    const maxRetries = 3;
    let currentRetry = 1;
    while (maxRetries >= currentRetry){
        try {
            await browser.runtime.sendMessage({ action: 'noop' });
            return true;
        } catch (error) {
            if (error instanceof Error && error.message.includes('Could not establish connection')){
                console.warn(`background script idle.. retry ${currentRetry}/${maxRetries}`, error);
            } else {
                throw error;
            }
        }
        await new Promise((resolve) => setTimeout(resolve, 100));
    }
    return false;
}