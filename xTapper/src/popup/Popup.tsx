import { useState } from "preact/hooks";
import { sendAction, useSendAction } from "../messaging";
import Stats from "./components/Stats";
import { t } from "../utils";

export default function Popup() {
    const [isRecording, setIsRecording] = useState(false);

    useSendAction({ action: 'getIsRecording' }, [], setIsRecording);

    const toggleRecording = () => {
        sendAction({ action: 'toggleRecording', payload: !isRecording })
            .then(x => setIsRecording(x))
            .catch(console.error);
    };

    function openConfig(e: MouseEvent) {
        e.preventDefault();
        browser.tabs.create({
            url: browser.runtime.getURL('options.html'),
        });
    }
    function openLists(e: MouseEvent) {
        e.preventDefault();
        browser.tabs.create({
            url: browser.runtime.getURL('options.html#lists'),
        });
    }
    return (
        <>
            <h1>xTapper</h1>
            <div class="status">
                <button class={`toggle ${isRecording ? 'on' : 'off'}`} onClick={toggleRecording}></button>
                <p id="status-text">{ isRecording ? t('recording_status_on') : t('recording_status_off') }</p>
            </div>
            <Stats />
            <footer>
                <a href="#" onClick={openLists}>{t('lists')}</a>
                <a href="#" onClick={openConfig}>{t('config')}</a>
            </footer>
        </>
    );
}