import { useState } from "preact/hooks";
import { Stats } from "../../types";
import { useSendAction } from "../../messaging";
import { t } from "../../utils";

export default function StatsComponent() {
    const [stats, setStats] = useState<Stats | null>(null);
    const [serverUrl, setServerUrl] = useState('');

    useSendAction({ action: 'getStats' }, [], (response) => {
        setStats(response);
    });

    useSendAction({ action: 'getOptions' }, [], (options) => {
        setServerUrl(options.syncServerUrl);
    });

    return (
        <div class="stats-container">
            <div class="stats stats-local">
                <h3>{t('local')}</h3>
                <div class="tweets">
                    <span><b class="count">{stats?.tweets ?? 'n/a'}</b> {t('tweets')}</span>
                </div>
                <div class="users">
                    <span><b class="count">{stats?.users ?? 'n/a'}</b> {t('users')}</span>
                </div>
            </div>
            <div class="stats stats-server">
                <h3>{t('sync_server')}</h3>
                {stats?.syncServerOnline && stats.syncServer ?
                    <>
                        <span class="url">{ serverUrl }</span>
                        <div class="tweets">
                            <span><b class="count">{stats.syncServer.tweets ?? 'n/a'}</b> {t('tweets')}</span>
                        </div>
                        <div class="users">
                            <span><b class="count">{stats.syncServer.users ?? 'n/a'}</b> {t('users')}</span>
                        </div>
                    </>
                    : <div class="server-error">
                        {
                            serverUrl ? <p>{t('sync_server_offline')}</p> : <p>{t('sync_server_unset')}</p>
                        }
                        <p>{t('local_mode')}</p>
                    </div>
                }
            </div>
        </div>
    );
}