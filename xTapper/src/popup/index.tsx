import { render } from 'preact';
import '../main.css';
import './popup.css';
import Popup from './Popup';

async function start(){
    render(<Popup />, document.getElementById('app')!);
}

start();