export type TweetObserverCallback = (tweetArticle: HTMLElement[]) => void;

export default class TweetObserver {
    cb: TweetObserverCallback;
    observer: MutationObserver;

    constructor(cb: TweetObserverCallback) {
        this.cb = cb;
        this.observer = new MutationObserver((mutations) => {
            const tweets: HTMLElement[] = [];
            mutations.forEach((mutation) => {
                if (mutation.addedNodes && mutation.addedNodes.length > 0) {
                    for (let i = 0; i < mutation.addedNodes.length; i++) {
                        const newNode = (mutation.addedNodes[i]) as HTMLElement;
                        const tweetArticle = newNode.querySelector('article');
                        if (tweetArticle){
                            tweets.push(tweetArticle);
                        }
                    }
                }
            });
            if (tweets.length){
                cb(tweets)
            }
        });
    }

    start() {
        const main = document.querySelector('main');
        if (main){
            this.observer.observe(main, {
                childList: true,
                subtree: true,
            });
        }
    }

    stop() {
        this.observer.disconnect()
    }
}