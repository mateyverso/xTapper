import { render } from 'preact';
import { App } from './app';
import JunkObserver from './JunkObserver';
import { sendAction } from '../messaging';
import './app.css';

document.addEventListener('keydown', function(e){
    if (e.key === 'Escape' && e.ctrlKey){
        toggleToolbox();
    }
});

document.body.addEventListener('toggle-toolbox', () => {
    toggleToolbox();
})

function toggleToolbox(){
    let container = document.getElementById('bookmarker-toolbox-container');
    if (!container){
        console.log('no container, creating')
        container = document.createElement('div');
        container.setAttribute('id', 'bookmarker-toolbox-container');
        document.querySelector('header')?.prepend(container);
    }
    if (container.childElementCount === 0){
        render(<App />, container)
    } else {
        render(null, container)
    }
}

async function load(){
    const options = await sendAction({ action: 'getOptions' });
    if (options.hideJunk){
        const junkObserver = new JunkObserver();
        junkObserver.start();
        junkObserver.exec();
    }
    toggleToolbox();
}

load();