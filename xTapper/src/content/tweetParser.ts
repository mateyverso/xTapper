export interface Tweet_FE {
    id: string,
    parent: string | null,
    user: string,
    date: string,
    text: string,
    responses: number,
    retweets: number,
    likes: number,
    impressions: string,
    bookmarks: number | null,
    mentions: string[],
    links: string[],
    hashtags: string[],
    pictures: Picture[]
}

export interface Picture {
    alt?: string,
    url: string
}

export function getPermalinkInfo(article: HTMLElement): { user: string, id: string, date: string | null } | null {
    const permalinkAnchor = article.querySelector('a:has(time)');
    if (!permalinkAnchor){
        return null;
    }
    const date = permalinkAnchor.querySelector('time')!.getAttribute('datetime');
    const permalink = permalinkAnchor.getAttribute('href') || '';
    const permalinkMatch = permalink.match(/^\/(.*)\/status\/(.*)/);
    if (!permalinkMatch){
        throw new Error('permalink invalid format');
    }
    const [,user,id] = permalinkMatch;
    return { id, user, date };
}

/**
 * @todo improve some fields such as counts ("180mil" likes will get 180)
 */
export function mapDomToTweet(article: HTMLElement): Tweet_FE | null {
    const permalinkInfo = getPermalinkInfo(article);
    if(!permalinkInfo) return null;
    const { user, id, date } = permalinkInfo;
    const tweetTextContainer = article.querySelector('[data-testid="tweetText"]');
    const statsElements = Array.from(article.querySelectorAll('[data-testid="app-text-transition-container"]'));
    let impressions = '';
    if (statsElements.length === 5) {
        impressions = statsElements[0].textContent ?? 'error';
    } else if (statsElements.length === 4) {
        impressions = statsElements[3].textContent ?? 'error';
    } else {
        throw new Error('unknown scenario');
    }
    const bookmarks = article.querySelector('[data-testid="bookmark"]')?.textContent;
    const links = [...new Set(Array.from(article.querySelectorAll('[rel="noopener noreferrer nofollow"]'))
        .map(el => el.getAttribute('href') || ''))];
    const hashtags = Array.from(article.querySelectorAll('a[href^="/hashtag"]')).map(el => el.textContent || '');
    const pictures = Array.from(article.querySelectorAll('[data-testid="tweetPhoto"] img')).map(el => ({
        alt: el.getAttribute('alt') || '',
        url: el.getAttribute('src') || ''
    }))
    const mentions = tweetTextContainer ? 
        Array.from(tweetTextContainer?.querySelectorAll('a'))
            .map(el => el.textContent || '')
            .filter(x => x?.startsWith('@')) 
        : [];
    return {
        id,
        parent: '',
        user,
        date: date || '',
        text: tweetTextContainer?.textContent ?? '',
        responses: parseInt(article.querySelector('[data-testid="reply"]')?.textContent || '0'),
        retweets: parseInt(article.querySelector('[data-testid="retweet"]')?.textContent || '0'),
        likes: parseInt(article.querySelector('[data-testid="like"]')?.textContent || '0'),
        impressions,
        bookmarks: bookmarks ? parseInt(bookmarks) : null,
        mentions,
        links,
        hashtags,
        pictures
    }
}