//TODO: check performance
export default class JunkObserver {
    observer: MutationObserver;
    junkSelectors = [
        '[data-testid="sidebarColumn"]',
        '[data-testid="DMDrawer"]'
    ];
    constructor() {
        this.observer = new MutationObserver((mutations) => {
            mutations.forEach((mutation) => {
                if (mutation.addedNodes && mutation.addedNodes.length > 0) {
                    for (let i = 0; i < mutation.addedNodes.length; i++) {
                        const newNode = (mutation.addedNodes[i]) as HTMLElement;
                        const junk = this.junkSelectors
                            .map(selector => newNode.querySelector(selector))
                            .filter(x => !!x);
                        junk.forEach(element => element && element.classList.add('hidden'))
                    }
                }
            });
        });
    }

    exec() {
        this.junkSelectors.forEach(selector => {
            document.querySelectorAll(selector).forEach(e => {
                e.classList.add('hidden');
            });
        });
    }

    start() {
        const main = document.body;
        if (main){
            this.observer.observe(main, {
                childList: true,
                subtree: true,
            });
        }
    }

    stop() {
        this.observer.disconnect()
    }
}