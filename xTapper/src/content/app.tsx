import { useState, useEffect } from 'preact/hooks'
import './app.css';
import SidebarListItem from './components/SidebarListItem';
import Key from './components/Key';
import useTweetBookmarkAction from './hooks/useTweetBookmarkAction';
import ListForm from './components/ListForm';
import { List } from '../types';
import BookmarkLabel from './components/BookmarkLabel';
import { sendAction } from '../messaging';
import { t } from '../utils';

export function App() {

  function refreshLists() {
    sendAction({ action: 'getLists' }).then((r) => {
      setLists(r);
    }).catch(err => console.error(err))
  }

  function getNewEmptyList(): Partial<List> {
    const defaultColors = ['#5869e4', '#1a815b', '#811a36', '#814f1a', '#391a81', '#1a817c', '#2d811a', '#0e3c65', '#58e461']
    return {
      name: '',
      color: defaultColors[lists.length],
      description: '',
      order: lists.length + 1
    }
  }

  useEffect(() => {
    refreshLists();
  }, []);
  const [lists, setLists] = useState<List[]>([]);
  const [currentList, setCurrentList] = useState<List | null>(null);
  const [isEditingList, setIsEditingList] = useState<boolean>(false);
  const [editingList, setEditingList] = useState<Partial<List> | null>(null);

  function hotkeysHandler(e: KeyboardEvent) {
    if (e.key === 'Escape') {
      setCurrentList(null);
    }
    if (Number.isInteger(+e.key)) {
      const selectedIndex = +e.key - 1;
      if (lists[selectedIndex]) {
        setCurrentList(lists[selectedIndex])
      }
    }
  }

  useEffect(() => () => console.log('leave'), []);

  useEffect(() => {
    document.addEventListener('keydown', hotkeysHandler);
    return () => {
      document.removeEventListener('keydown', hotkeysHandler)
    }
  }, [lists]);

  useEffect(() => {
    const root = document.querySelector(':root') as HTMLElement;
    root.style.setProperty('--bookmarker-list-color-active', currentList ? currentList.color : 'transparent')
  }, [currentList])

  useTweetBookmarkAction(currentList, lists);

  async function onSubmitList(l: List, id?: string) {
    await sendAction( id ? 
      { action: 'updateList', payload: { id, data: l } }   :
      { action: 'createList', payload: l }
    )
    refreshLists();
    setEditingList(null);
    setIsEditingList(false);
  }

  function onClose(e: MouseEvent) {
    e.preventDefault();
    const event = new Event('toggle-toolbox');
    document.body.dispatchEvent(event);
  }

  async function onDeleteList(id: string) {
    await sendAction({ action: 'deleteList', payload: { id } });
    refreshLists();
    setEditingList(null);
    setIsEditingList(false);
  }

  return (
    <div class="bookmarker-toolbox" id="bookmarker-toolbox">
      <h1>{ t('extensionName') }</h1>
      {isEditingList || editingList ? null :
        <div class="toolbar-actions">
          <a href="#" onClick={(e) => {
            e.preventDefault();
            setEditingList(getNewEmptyList());
          }}>{t('add_list')}</a>
          <a href="#" onClick={(e) => {
            e.preventDefault();
            setIsEditingList(true);
          }}>{t('edit_list')}</a>
          <div class="separator" />
          <a href="#" onClick={onClose}>{t('close')}</a>
        </div>
      }
      <div class={`current-list ${editingList || isEditingList ? 'hidden' : ''}`}>
        <div>
          <label>{t('current_list')}:</label>
          {currentList ? <a href="#" onClick={(e) => {
            e.preventDefault();
            setCurrentList(null);
          }}>{t('clear_selection')}</a> : null}
        </div>
        {currentList ?
          <BookmarkLabel color={currentList.color} text={currentList.name} /> :
          <BookmarkLabel color="transparent" text={t('no_active_list')} />
        }

      </div>
      <div class={`list-of-lists ${editingList || isEditingList ? 'hidden' : ''}`}>
        <ul>
          {
            lists.map(list => <SidebarListItem {...list}
              onSelect={() => {
                setCurrentList(list)
              }}
              isActive={currentList ? currentList.name === list.name : false}
            />)
          }
        </ul>
      </div>
      <div class={`${ isEditingList || editingList ? 'form-container' : ''}`}>
        {isEditingList && !editingList ? <div><small>{t('select_list_to_edit')}</small></div> : null}
        {isEditingList ? <select onChange={(e) => {
          if(!e.currentTarget.value){
            setEditingList(null);
          } else {
            const toEdit = lists.find(l => l.name === e.currentTarget.value);
            if (!toEdit) throw new Error(`list not found: ${e.currentTarget.value}`);
            setEditingList(toEdit);
          }
        }}>
          <option></option>
          {lists.map(l => (<option value={l.name}>{l.name}</option>))}
        </select> : null}
        {
          editingList ?
            <ListForm {...editingList} onSubmit={onSubmitList} /> :
            null
        }
        <div class="extra-buttons">
          {isEditingList || editingList ? <button onClick={() => {
            setEditingList(null);
            setIsEditingList(false);
          }}>
            {t('cancel')}
          </button> : null}
          {
            isEditingList && editingList && editingList.id ?
              <button onClick={() => {
                if (editingList.id &&
                  confirm(`Do you want to delete the list "${editingList.name}"?`)
                ) onDeleteList(editingList.id)
              }}> {t('delete')}
              </button> : null
          }
        </div>
      </div>
      <div class="help">
        <p>
          {t('press')} <Key value="Ctrl" /> + <Key value="Esc" /> {t('to_show_hide_panel')}.
        </p>
        <p>
          {t('keys')} <Key value="1" /> - <Key value="9" /> {t('to_activate_list')}. <Key value="Esc" /> {t('to_unselect')}.
        </p>
      </div>
    </div>
  )
}