import './Key.css';

export interface KeyProps {
    value: string
}

export default function Key(props: KeyProps){
    return <div class="key">
        { props.value}
    </div>
}