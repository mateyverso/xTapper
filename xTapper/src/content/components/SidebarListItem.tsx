import { List } from "../../types";
import BookmarkIcon from "./BookmarkIcon";
import Key from "./Key";
import './SidebarListItem.css';

export type SidebarListItemProps = List & {
    isActive?: boolean;
    onSelect?: () => void
}

export default function SidebarListItem(props: SidebarListItemProps){
    return <li class={props.isActive ? 'active' : ''}>
        <button onClick={props.onSelect}>
            <Key value={props.order.toString()}/>
            <span>{props.name}</span>
            <BookmarkIcon active={props.isActive} color={props.color} />
        </button>
    </li>
}