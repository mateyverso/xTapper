import { useState } from "preact/hooks";
import { List } from "../../types";
import './ListForm.css';
import { t } from "../../utils";

export type ListFormProps = Partial<List> & {
    onSubmit: (list: List, originalName?: string) => void
}

export default function ListForm(props: ListFormProps){

    const [id] = useState(props.id || undefined);
    const [name, setName] = useState(props.name || '');
    const [description, setDescription] = useState(props.description || '');
    const [color, setColor] = useState(props.color || '#000');
    const [order, setOrder] = useState(props.order || 0);
    
    const isNew = !id;
    
    function onSubmit(e: SubmitEvent){
        e.preventDefault();
        props.onSubmit({
            id: id ? id : crypto.randomUUID(),
            name,
            description,
            color,
            order,
            last_update: Date.now(),
        }, id);
    }

    return <form onSubmit={onSubmit} class="add-list-form">
        <h2>{ t(isNew ? 'new_list' : 'edit_list') }</h2>
        <input type="text" name="name" placeholder={t('name')} value={name} onInput={(e) => setName(e.currentTarget.value) } />
        
        <textarea rows={2} name="description" placeholder={t('description')} value={description} onInput={(e) => setDescription(e.currentTarget.value) } />
        <div class="row-color-number">
            <label for="color">{t('color')}</label>
            <input type="color" name="color" value={color} onInput={(e) => setColor(e.currentTarget.value) } />
            <label for="order">{t('order')}</label>
            <input type="number" name="order" value={order} onInput={(e) => setOrder(parseInt(e.currentTarget.value)) } />
        </div>
        <div class="row-actions">
            <button type="submit">
                {t('save')}
            </button>
        </div>
    </form>
}