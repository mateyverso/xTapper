export interface BookmarkLabelProps {
    color: string;
    text: string;
    height?: string;
}

export default function BookmarkLabel(props: BookmarkLabelProps) {
    const height = props.height || '16px';
    return <div style={{
        boxSizing: 'border-box',
        borderLeft: '25px solid transparent',
        borderRight: 'none',
        borderTop: `${height} solid ${props.color}`,
        borderBottom: `${height} solid ${props.color}`,
        display: 'flex',
        alignItems: 'center',
        paddingLeft: '10px',
        height: 0,
        borderRadius: '2px',
    }}>
        { props.text }
    </div>
}