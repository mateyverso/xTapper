import { useEffect } from "preact/hooks";
import TweetObserver from "../TweetObserver";
import { getPermalinkInfo, mapDomToTweet } from "../tweetParser";
import { List } from "../../types";
import { sendAction } from "../../messaging";

const SELECTION_MAIN_CLASS = 'bkmrk-selection';

function addBookmarkIndicator(element: HTMLElement, list: List) {
    if (element.querySelector(`[data-list-id="${list.id}"]`)){
        return;
    }
    const existingBookmarks = element.querySelectorAll('.xTapper-bookmark').length;
    const bookmark = document.createElement('div');
    bookmark.textContent = list.order.toString();
    bookmark.style.backgroundColor = list.color;
    bookmark.style.right = `${40 + (existingBookmarks * 27)}px`;
    bookmark.dataset.listId = list.id;
    bookmark.className = 'xTapper-bookmark';
    element.appendChild(bookmark);
}

function removeBookmarkIndicator(element: HTMLElement, list: List){
    const bookmark = element.querySelector(`[data-list-id="${list.id}"]`);
    bookmark?.remove();
}

export default function useTweetBookmarkAction(currentList: List | null, lists: List[] | null){

    function tweetEvent(e: Event){
        if (!currentList){
            return;
        }
        e.stopPropagation();
        e.preventDefault();
        if (!e.currentTarget || !(e.currentTarget instanceof HTMLElement)) return;
        const tweetElement: HTMLElement = e.currentTarget;
        const tweet = mapDomToTweet(tweetElement);
        if (!tweet) {
            console.error(tweetElement);
            throw new Error('Failed to parse tweet')
        }
        sendAction({ action: 'selectTweet', payload: {
            id: tweet.id,
            list: currentList.id
        }}).then(response => {
            if (response){
                addBookmarkIndicator(tweetElement, currentList);
            } else {
                removeBookmarkIndicator(tweetElement, currentList);
            }
            console.log('selectTweet response', response);
        }).catch(err => console.error(err));
    }

    useEffect(() => {
        const mainElement = document.querySelector('main');
        if(!mainElement) throw new Error('main element not found - cannot attach SELECTION class')
        const tweets = Array.from(mainElement.querySelectorAll('article'));
        tweets.map(tw => tw.addEventListener('click', tweetEvent));
        if (currentList){
            mainElement.classList.add(SELECTION_MAIN_CLASS);
        } else {
            mainElement.classList.remove(SELECTION_MAIN_CLASS);
        }

        const tweetObserver = new TweetObserver(async tweetArticles => {
            tweetArticles.forEach(tweetArticle => {
                tweetArticle.addEventListener('click', tweetEvent)
            });
            const tweetIds = tweetArticles.map(x => getPermalinkInfo(x)?.id);
            const payload = tweetIds.filter(t => typeof t !== 'undefined') as string[];
            const listsByTweet = await sendAction({ action: 'getListsForTweets', payload });
            tweetIds.forEach((tweetId, index) => {
                if (tweetId && listsByTweet[tweetId]){
                    listsByTweet[tweetId].forEach(listId => {
                        const list = lists?.find(l => l.id === listId);
                        if(list){
                            addBookmarkIndicator(tweetArticles[index], list);
                        }
                    })
                }
            });
        });
        tweetObserver.start();

        return () => {
            mainElement.classList.remove(SELECTION_MAIN_CLASS);
            tweets.map(tw => tw.removeEventListener('click', tweetEvent));
            tweetObserver.stop();
        }
    }, [currentList, lists])

}