import { TweetLite } from '@x-tapper/x-tractor/types/TweetLite';
import { UserLite } from '@x-tapper/x-tractor/types/UserLite';

export type User = UserLite;
export type Tweet = TweetLite;

export type Stats = {
    tweets: number,
    users: number,
    syncServerOnline: false,
} | {
    tweets: number,
    users: number,
    syncServerOnline: true,
    syncServer: {
        tweets: number,
        users: number,
    }
};

export type Options = {
    syncServerUrl: string,
    captureRtAndFav: boolean,
    captureFollows: boolean,
    hideJunk: boolean,
};

export type TweetFavorite = {
    id: string,
    tweet_id: string,
    user_id: string,
    last_update: number,
};

export type TweetRetweet = {
    id: string,
    tweet_id: string,
    user_id: string,
    last_update: number,
};

export type UserFollower = {
    id: string,
    user_id: string,
    follower_id: string,
    last_update: number,
};

export type UserFollowing = {
    id: string,
    user_id: string,
    following_id: string,
    last_update: number,
};

export interface List {
    id: string,
    order: number,
    name: string,
    description: string,
    color: string,
    last_update: number,
}

export interface ListToTweet {
    list_id: string,
    tweet_id: string,
    last_update: number,
};