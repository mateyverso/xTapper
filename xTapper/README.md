# xTapper

xTapper is a Firefox Extension that acts as a passive scraper for twitter/x. It saves structured data while you browse the site.

## Highlights

- Export tweets and users as CSV
- Bookmark tweets into user-defined lists
    - export lists to CSV
    - visualize lists contents
- Option to hide the right sidebar of the web ui (premium, trending, who to follow). 
- All data is saved locally into indexed-db, can be exported to CSV. 
- A [sync-server](../xSyncServer/README.md) can be optionally configured to mirror the data.

## Installation and Usage

Download the .xpi file from [Releases](https://codeberg.org/mateyverso/xTapper/releases) and add it to Firefox.

The extension needs to intercept requests in twitter/x in order to work.
You can allow for this behavior clicking the extension icon -> Manage Extension -> Permissions -> Allow for twitter / x domains.

Interception and recording of data can be toggled right from the extension icon.

Use `Ctrl` + `Esc` to toggle right sidebar containing lists.

## Development

This extension was built using NodeJS 18+ and [pnpm](https://pnpm.io/installation).

```bash
pnpm install

# dev
pnpm run watch

# generate xpi
pnpm run build # -> ./dist
pnpm run bundle # -> ./release
```

You can add the extension for debug in firefox going to [add-on debugging](about:debugging#/runtime/this-firefox) and "Load temporary extension..". Then pick the `./dist/manifest.json` file.

## TODOS

- Separate vite.config for options and content
    - If in same config, chunking is activated and a new assets/jsxRuntime-* is generated and I don't know how to include it, nor how to disable it.